import '@babel/polyfill';
import { pathOr, zipObj, keys, pipe, assocPath, pick } from 'ramda';
import htmlescape from 'htmlescape';
import debug from 'debug';

const loginfo = debug('webapp');

const renderHtml = (config, assets, i18n, settings, stylesheetUrl, app) => `
    <!doctype html>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" href="${app.favicon}">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:400">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow:700">
        <link rel="stylesheet" href="/css/preloader.css">
        <link rel="stylesheet" href="/static/css/vendors~main.chunk.css">
        <style id="insertion-point-jss"></style>
        <link rel="stylesheet" href="${stylesheetUrl}">
        <title>${app.title}</title>
        <script src="https://cdn.jsdelivr.net/npm/text-encoding@0.6.4/lib/encoding.min.js"></script>
        <script> CONFIG = ${htmlescape(config)} </script>
        <script> SETTINGS = ${htmlescape(settings)} </script>
        <script> I18N = ${htmlescape(i18n)} </script>
      </head>
      <body>
        <noscript>
          You need to enable JavaScript to run this app.
        </noscript>
        <div id="root">
          <div class="loader"></div>
        </div>
        <script type="text/javascript" src="${assets.vendors}"></script>
        <script type="text/javascript" src="${assets.main}"></script>
      </body>
    </html>
  `;

const ssr = ({ config, assets, configProvider }) => async (req, res) => {
  const { tenant } = req;
  const settings = await configProvider.getSettings(tenant);
  const datasources = await configProvider.getDataSources(tenant);
  const tenantDatasources = pick(pathOr([], ['sdmx', 'datasourceIds'], settings), datasources);
  const locales = pipe(pathOr([], ['i18n', 'locales']), keys)(settings);
  const i18n = await configProvider.getI18n(tenant, locales);
  const stylesheetUrl = settings.styles;
  const html = renderHtml(
    {
      tenant,
      keycloak: { ...tenant.keycloak, url: `${config.authServerUrl}/auth` },
      env: config.env,
    },
    assets,
    zipObj(locales, i18n),
    assocPath(['sdmx', 'datasources'], tenantDatasources, settings),
    stylesheetUrl,
    {
      title: pathOr('Data Explorer', ['app', 'title'], settings),
      favicon: pathOr('/favicon.ico', ['app', 'favicon'], settings),
    },
  );
  res.send(html);
  loginfo(`render site for tenant '${tenant.name}'`);
};

export default ssr;
