const { getMessage } = require('../connector');

describe('server | services | healthcheck | connector  ', () => {
  describe('getMessage', () => {
    it('should return the right result', () => {
      const fakeReq = {
        body: {},
        path: '/healthcheck',
        method: 'FakeMethod',
      };
      const result = getMessage(fakeReq);
      const expected = {
        service: 'healthcheck',
        method: 'get',
      };
      expect(result).toEqual(expected);
    });
  });
});
