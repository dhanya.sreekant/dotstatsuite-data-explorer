import debug from 'debug';
import params from '../params';
import ConfigProvider from '../configProvider';

const loginfo = debug('wabapp:config');

const init = () => {
  loginfo(`running "${params.env}" env`);
  if (process.env.NODE_ENV !== 'test') console.log(params); // eslint-disable-line no-console
  return Promise.resolve({ config: params, configProvider: ConfigProvider(params) });
};

export default init;
