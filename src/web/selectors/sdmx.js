import { createSelector } from 'reselect';
import * as R from 'ramda';
import * as SDMXJS from '@sis-cc/dotstatsuite-sdmxjs';
import Set from 'es6-set';
import dateFns from 'date-fns';
import fr from 'date-fns/locale/fr';
import en from 'date-fns/locale/en';
import es from 'date-fns/locale/es';
import ar from 'date-fns/locale/ar';
import it from 'date-fns/locale/it';
import { rules } from '@sis-cc/dotstatsuite-components';
import {
  getSdmxPeriod,
  getFrequencies,
  parseInclusivesDates,
  changeToDates,
  checkDatesIsAsc,
  changeSdmxPeriod
} from '../lib/sdmx/frequency';
import { getDatasource, sdmxRange, locales, defaultFrequency, sdmxPeriod, sdmxPeriodBoundaries } from '../lib/settings';
import {
  getLastNObservations,
  getLocale,
  getParams,
  getFrequency,
  getDataAvailability,
  getDataquery,
  getDataflow,
} from './router';
import { getFilename, getOnlyHasDataDimensions } from '../lib/sdmx';

//------------------------------------------------------------------------------------------------#0
const getSdmx = R.prop('sdmx');
//------------------------------------------------------------------------------------------------#1
export const getDimensions = createSelector(getSdmx, R.prop('dimensions'));

export const getContentConstraints = createSelector(getSdmx, R.prop('contentConstraints'));

export const getHasContentConstraints = createSelector(getSdmx, R.prop('hasContentConstraints'));

export const getData = createSelector(getSdmx, R.prop('data'));

export const getDataRange = createSelector(getSdmx, R.prop('range'));

export const getTimePeriod = createSelector(getSdmx, R.prop('timePeriod'));

export const getFrequencyArtefact = createSelector(getSdmx, R.propOr({}, 'frequencyArtefact'));

export const getExternalResources = createSelector(getSdmx, R.prop('externalResources'));

//------------------------------------------------------------------------------------------------#2
export const getNotApplyDataAvailabilty = createSelector(
  getDataAvailability,
  getHasContentConstraints,
  (dataAvailabilityIsOff, hasCC) => R.or(dataAvailabilityIsOff, R.not(hasCC))
);

//------------------------------------------------------------------------------------------------#3
export const getDatesBoundaries = createSelector(
  getSdmx,
  getFrequency,
  getNotApplyDataAvailabilty,
  (sdmx, frequency, notApplyDataAvailabilty) =>
    R.converge(
      (includingBoundaries, timePeriodBoundaries = []) => {
        if (notApplyDataAvailabilty) return R.map(dateFns.parse)(sdmxPeriodBoundaries);
        return R.pipe(
          parseInclusivesDates(includingBoundaries, frequency),
          changeToDates(sdmxPeriodBoundaries),
          checkDatesIsAsc,
        )(timePeriodBoundaries);
      },
      [R.prop('timePeriodIncludingBoundaries'), R.prop('timePeriodBoundaries')],
    )(sdmx),
);

export const getContentConstrainedDimensions = createSelector(
  getDimensions,
  getNotApplyDataAvailabilty,
  (dimensions, notApplyDataAvailabilty) => {
    if (notApplyDataAvailabilty) return dimensions;
    return getOnlyHasDataDimensions(dimensions);
  }
);

export const getFrequencyArtefactContentConstrained = createSelector(
  getFrequencyArtefact,
  getNotApplyDataAvailabilty,
  (frequencyArtefact, notApplyDataAvailabilty) => {
    if (notApplyDataAvailabilty) return frequencyArtefact;
    if (R.prop('isAttribute')(frequencyArtefact)) return frequencyArtefact;
    return getOnlyHasDataDimensions([frequencyArtefact])
  },
);

export const getDataflowName = createSelector(getSdmx, ({ name, data }) =>
  R.isNil(data) ? name : R.path(['structure', 'name'], data),
);

export const getRawStructureRequestArgs = createSelector(
  getDataflow,
  getLocale,
  (dataflow, locale) => ({
    datasource: getDatasource(R.prop('datasourceId', dataflow)),
    identifiers: {
      code: R.prop('dataflowId', dataflow),
      ...R.pick(['agencyId', 'version'], dataflow),
    },
    locale,
  }),
);

export const getStructureRequestArgs = createSelector(getRawStructureRequestArgs, args =>
  SDMXJS.getRequestArgs({ ...args, type: 'dataflow' }),
);

//------------------------------------------------------------------------------------------------#4
export const getDimensionsWithDataQuerySelection = createSelector(
  getDimensions,
  getDataquery,
  R.pipe(
    R.useWith(
      (filters, dataquery) =>
        R.addIndex(R.map)((filter, index) => {
          if (R.isEmpty(filter)) return filter;
          if (R.pipe(R.nth(index), R.anyPass([R.isEmpty, R.isNil]))(dataquery)) return filter;
          const valueIdsSet = new Set(R.pipe(R.nth(index), R.split('+'))(dataquery));
          return R.over(
            R.lensProp('values'),
            R.map(
              R.ifElse(({ id }) => valueIdsSet.has(id), R.assoc('isSelected', true), R.identity),
            ),
            filter,
          );
        }, filters),
      [R.identity, R.ifElse(R.isNil, R.always([]), R.split('.'))],
    ),
    R.reject(
      R.anyPass([
        R.pipe(R.prop('display'), R.not),
        R.pipe(R.prop('id'), rules.isFreqDimension),
        R.pipe(R.prop('values'), R.length, R.gte(1)),
      ]),
    ),
  ),
);

export const getRefAreaDimension = createSelector(
  getContentConstrainedDimensions,
  R.find(R.pipe(R.prop('id'), rules.isAreaDimension)),
);

export const getSelection = createSelector(
  getDimensionsWithDataQuerySelection,
  R.pipe(
    R.map(R.over(R.lensProp('values'), R.filter(R.propEq('isSelected', true)))),
    R.filter(R.pipe(R.prop('values'), R.length, R.flip(R.gt)(0))),
  ),
);

export const getAvailableFrequencies = createSelector(
  getFrequencyArtefactContentConstrained,
  getFrequencies
);

//------------------------------------------------------------------------------------------------#5
export const getPeriod = createSelector(
  getParams,
  getFrequency,
  getDatesBoundaries,
  (params, frequency = defaultFrequency, boundaries) =>
    R.pipe(
      R.prop('period'),
      R.ifElse(
        R.pipe(R.length, R.equals(2)),
        changeSdmxPeriod(frequency, boundaries),
        R.always(changeSdmxPeriod(frequency, boundaries)(sdmxPeriod)),
      ),
    )(params),
);

export const getTimelineAxisProc = createSelector(getFrequency, getLocale, (frequency, locale) => {
  if (frequency === 'M') {
    const format = R.pathOr('YYYY MMM', [locale, 'timeFormat'], locales);
    const dateFnsLocales = { ar, en, es, fr, it };
    const dateLocale = R.ifElse(R.has(locale), R.prop(locale), R.prop('en'))(dateFnsLocales);
    return date => dateFns.format(date, format, { locale: dateLocale });
  }
  return date => getSdmxPeriod(frequency, date);
});

//------------------------------------------------------------------------------------------------#6
export const getDataRequestParams = createSelector(
  getPeriod,
  getLastNObservations,
  (period, lastNObservations) => ({
    startPeriod: R.head(period),
    endPeriod: R.last(period),
    lastNObservations,
  }),
);

export const getRawDataRequestArgs = createSelector(
  getRawStructureRequestArgs,
  getDataquery,
  getDataRequestParams,
  (args, dataquery, params) => ({
    ...args,
    dataquery,
    params,
    ...(R.path(['datasource', 'hasRangeHeader'], args) ? { range: sdmxRange } : {}),
  }),
);

export const getDataRequestArgs = createSelector(getRawDataRequestArgs, args =>
  SDMXJS.getRequestArgs({ ...args, type: 'data' }),
);

export const getFullDataflowFileRequestArgs = dataflow =>
  createSelector(
    getLocale,
    R.pipe(
      locale => ({
        datasource: getDatasource(R.prop('datasourceId', dataflow)),
        identifiers: {
          code: R.prop('dataflowId', dataflow),
          ...R.pick(['agencyId', 'version'], dataflow),
        },
        locale,
        asFile: true,
        format: 'csv',
        labels: 'both',
        type: 'data',
      }),
      R.converge(({ url, headers, params }, filename) => ({ url, headers, params, filename }), [
        SDMXJS.getRequestArgs,
        getFilename,
      ]),
    ),
  );

export const getDataFileRequestArgs = display =>
  createSelector(
    getRawDataRequestArgs,
    R.converge(({ url, headers, params }, filename) => ({ url, headers, params, filename }), [
      args =>
        SDMXJS.getRequestArgs({
          ...R.omit(['range'], args),
          asFile: true,
          format: 'csv',
          labels: R.when(R.always(display === 'code'), R.always('code'))('both'),
          type: 'data',
        }),
      getFilename,
    ]),
  );

export const getDataSourceHeaders = createSelector(getDataRequestArgs, R.prop('headers'));

export const getStructureUrl = createSelector(getRawStructureRequestArgs, args =>
  SDMXJS.getSDMXUrl({ ...args, type: 'dataflow' }),
);

export const getDataUrl = ({ agnostic }) =>
  createSelector(getRawDataRequestArgs, args =>
    SDMXJS.getSDMXUrl({ ...args, agnostic, type: 'data' }),
  );
