import { createSelector } from 'reselect';
import * as R from 'ramda';
import bs62 from 'bs62';
import { getTerm, getConstraints as getRouterConstraints, getStart, getParams } from './router';
import { search } from '../lib/settings';
import { getVisUrl } from '../utils/router';

//------------------------------------------------------------------------------------------------#0
export const getSearch = R.prop('search');

//------------------------------------------------------------------------------------------------#1
export const getConfig = createSelector(getSearch, R.prop('config'));

export const getNumFound = createSelector(getSearch, R.prop('numFound'));

export const getFacets = createSelector(getSearch, R.propOr([], 'facets'));

export const getRows = createSelector(getSearch, R.prop('rows'));

//------------------------------------------------------------------------------------------------#2
export const getConfigFacets = createSelector(getConfig, R.propOr([], 'facets'));

export const getHasNoSearchParams = createSelector(
  getTerm,
  getRouterConstraints,
  (term, constraints) =>
    R.and(R.anyPass([R.isEmpty, R.isNil])(term), R.anyPass([R.isEmpty, R.isNil])(constraints)),
);

export const getConstraints = createSelector(
  getFacets,
  R.pipe(
    R.filter(R.pipe(R.prop('count'), R.flip(R.gt)(0))),
    R.map(R.over(R.lensProp('values'), R.filter(R.propEq('isSelected', true)))),
  ),
);

export const getResultsFacets = createSelector(getFacets, facets => {
  const pinnedFacetIds = R.propOr([], 'pinnedFacetIds', search);
  const excludedFacetIds = R.propOr([], 'excludedFacetIds', search);
  return R.pipe(
    R.reject(R.pipe(R.prop('id'), bs62.decode, R.flip(R.includes)(excludedFacetIds))),
    R.reduce((memo, facet) => {
      const index = R.indexOf(bs62.decode(R.prop('id', facet)), pinnedFacetIds);
      if (R.equals(index, -1)) return R.append(facet, memo);
      return R.insert(index, R.assoc('isPinned', true, facet), memo);
    }, []),
  )(facets);
});

export const getDataflows = createSelector(getSearch, getParams, ({ dataflows = [] }, params) =>
  R.map(dataflow => R.assoc('url', getVisUrl(params, dataflow), dataflow))(dataflows),
);

export const getCurrentRows = createSelector(
  getRows,
  getDataflows,
  (rows, dataflows) => (R.isNil(rows) ? R.length(dataflows) : rows), // in case of an undefined rows by configs, doesn't work for the last page ...
);

//------------------------------------------------------------------------------------------------#3
export const getPages = createSelector(getCurrentRows, getNumFound, (rows, numFound) => {
  if (R.isNil(rows)) {
    return undefined;
  }
  return parseInt(numFound / rows) + (numFound % rows !== 0 ? 1 : 0);
});

export const getPage = createSelector(getStart, getCurrentRows, (start, rows) => {
  if (R.isNil(rows)) {
    return undefined;
  }
  return start / rows + 1;
});
