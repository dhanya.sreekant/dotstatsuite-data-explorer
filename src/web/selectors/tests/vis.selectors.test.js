import * as S from '../.';

const data = observation => ({ data: { structure: { dimensions: { observation } } } });
describe('selectors | vis', () => {
  describe('getDataDimensions', () => {
    test('no crash with partial element', () => {
      expect(S.getDataDimensions()({})).toEqual([]);
      expect(S.getDataDimensions()({ sdmx: data([{}]) })).toEqual([{ index: 0, values: [] }]);
      expect(S.getDataDimensions()({ sdmx: data([{ values: [{}, {}] }]) })).toEqual([
        { index: 0, values: [{ index: 0 }, { index: 1 }] },
      ]);
    });
  });
  describe('getVisDataDimensions', () => {
    test('separate one values and many values in two groups', () => {
      const state = {
        sdmx: data([
          { id: 'test1', values: [{}, {}] },
          { id: 'test2', values: [{}] },
        ]),
      };
      const expected = {
        many: { test1: { id: 'test1', index: 0, values: [{ index: 0 }, { index: 1 }] } },
        one: { test2: { id: 'test2', index: 1, values: [{ index: 0 }] } },
      };
      expect(S.getVisDataDimensions()(state)).toEqual(expected);
    });
  });
  describe('getVisActionId', () => {
    test('basic ouput', () => {
      expect(S.getVisActionId()({ vis: { actionId: 1 } })).toEqual(1);
      expect(S.getVisActionId()({ vis: {} })).toEqual(undefined);
    });
  });
  describe('getIsFull', () => {
    test('basic ouput', () => {
      expect(S.getIsFull()({ vis: { isFull: 1 } })).toEqual(1);
      expect(S.getIsFull()({ vis: {} })).toEqual(undefined);
    });
  });
  describe('getVisLayout', () => {
    test('basic ouput', () => {
      expect(S.getVisLayout({ vis: { layout: 1 } })).toEqual(1);
      expect(S.getVisLayout({ vis: {} })).toEqual(undefined);
    });
  });
  describe('getTimeDimensionInverted', () => {
    test('basic ouput', () => {
      expect(S.getTimeDimensionInverted()({ vis: { timeDimensionInverted: 1 } })).toEqual(1);
      expect(S.getTimeDimensionInverted()({ vis: {} })).toEqual(undefined);
    });
  });
  describe('getShareState', () => {
    test('basic ouput', () => {
      expect(S.getShareState()({ vis: { share: 1 } })).toEqual(1);
      expect(S.getShareState()({ vis: {} })).toEqual(undefined);
    });
  });
  describe('getShareMode', () => {
    test('basic ouput', () => {
      expect(S.getShareMode()({ vis: { share: { mode: 1 } } })).toEqual(1);
      expect(S.getShareMode()({ vis: {} })).toEqual(undefined);
    });
  });
  describe('getShareMail', () => {
    test('basic ouput', () => {
      expect(S.getShareMail()({ vis: { share: { mail: 1 } } })).toEqual(1);
      expect(S.getShareMail()({ vis: {} })).toEqual(undefined);
    });
  });
  describe('getIsSharing', () => {
    test('basic ouput', () => {
      expect(S.getIsSharing()({ vis: { share: { isSharing: 1 } } })).toEqual(1);
      expect(S.getIsSharing()({ vis: {} })).toEqual(undefined);
    });
  });
  describe('getHasShared', () => {
    test('basic ouput', () => {
      expect(S.getHasShared()({ vis: { share: { hasShared: 1 } } })).toEqual(1);
      expect(S.getHasShared()({ vis: {} })).toEqual(undefined);
    });
  });
  describe('getShareError', () => {
    test('basic ouput', () => {
      expect(S.getShareError()({ vis: { share: { error: 1 } } })).toEqual(1);
      expect(S.getShareError()({ vis: {} })).toEqual(undefined);
    });
  });
});
