import * as R from 'ramda';
import * as S from '../.';
import * as Sdmx from '../sdmx';

jest.mock('../../lib/settings', () => ({
  defaultFrequency: 'A',
  sdmxPeriod: ['2010', '2015'],
  sdmxPeriodBoundaries: ['1970-01-01T00:00:00.000', '2050-01-01T00:00:00.000'],
  theme: { visFont: 'totot' },
}));

describe('selectors | for data and structure', () => {
  describe('getDataDimensions', () => {
    test("no crash if data doesn't exist", () => {
      expect(S.getDataDimensions()({})).toEqual([]);
    });
    test("no crash if values doesn't have values", () => {
      const data = { structure: { dimensions: { observation: [{}] } } };
      const expected = [{ index: 0, values: [] }];
      expect(S.getDataDimensions()({ sdmx: { data } })).toEqual(expected);
    });
    test('add index to dimensions', () => {
      const data = { structure: { dimensions: { observation: [{ values: [{}, {}] }] } } };
      const expected = [{ index: 0, values: [{ index: 0 }, { index: 1 }] }];
      expect(S.getDataDimensions()({ sdmx: { data } })).toEqual(expected);
    });
  });
  describe('getVisDataDimensions', () => {
    test('separate one values and many values in two groups', () => {
      const data = {
        structure: {
          dimensions: {
            observation: [
              { id: 'test1', values: [{}, {}] },
              { id: 'test2', values: [{}] },
            ],
          },
        },
      };
      const expected = {
        many: { test1: { id: 'test1', index: 0, values: [{ index: 0 }, { index: 1 }] } },
        one: { test2: { id: 'test2', index: 1, values: [{ index: 0 }] } },
      };
      expect(S.getVisDataDimensions()({ sdmx: { data } })).toEqual(expected);
    });
  });
  describe('getPeriod', () => {
    it('should fallback period fit regarding boundaries', () => {
      const state = props => ({
        sdmx: {
          hasContentConstraints: true,
          frequencyDimension: { index: 0 },
          timePeriodBoundaries: R.propOr([undefined, undefined], 'tPB')(props),
          timePeriodIncludingBoundaries: R.propOr([true, true], 'tPIB')(props),
        },
        router: {
          params: {
            frequency: R.prop('freq')(props),
            period: R.prop('period')(props),
            dataAvailability: R.pipe(R.propOr('off', 'wCC'), R.equals('off'))(props),
          },
        },
      });
      // without period
      expect(Sdmx.getPeriod(state({ tPB: ['1995', '1995'], tPIB: [false, false] }))).toEqual([
        undefined,
        undefined
      ]);
      expect(Sdmx.getPeriod(state({ tPB: ['1995-01-01T00:00:00.000Z', '1995-01-01T00:00:00.000Z']}))).toEqual(['1995', '1995']);
      expect(Sdmx.getPeriod(state({ tPB: ['2011-01-01T00:00:00.000Z', '2013-01-01T00:00:00.000Z']}))).toEqual(['2011', '2013']);
      expect(Sdmx.getPeriod(state({ period: ['2010-01-01T00:00:00.000Z', '2010-01-01T00:00:00.000Z'], wCC: 'on' }))).toEqual(['2010', '2010']);
      expect(Sdmx.getPeriod(state({ freq: 'D'}))).toEqual(['2010-01-01', '2015-01-01']);
    });
  });
  describe('getPeriodBoundaries', () => {
    it('should fallback to sdmxBoundaries if needed', () => {
      const state = (timePeriodBoundaries, timePeriodIncludingBoundaries) => ({
        sdmx: { timePeriodBoundaries, timePeriodIncludingBoundaries, hasContentConstraints: true },
        router: {
          params: {
            frequency: 'A',
            dataAvailability: 'false',
          },
        },
      });
      expect(Sdmx.getDatesBoundaries(state(['c', 'd'], [true, false]))).toEqual([
        new Date('1970-01-01T00:00:00.000'),
        new Date('2050-01-01T00:00:00.000'),
      ]);
      expect(Sdmx.getDatesBoundaries(state(['1990-01-01T00:00:00.000Z', '2000-01-01T00:00:00.000Z'], [true, true]))).toEqual([
        new Date('1990-01-01T00:00:00'),
        new Date('2000-01-01T00:00:00'),
      ]);
      expect(Sdmx.getDatesBoundaries(state(['1990-01-01T00:00:00.000Z', '2000-01-01T00:00:00.000Z'], [false, false]))).toEqual([
        new Date('1991-01-01T00:00:00'),
        new Date('1999-01-01T00:00:00'),
      ]);
      expect(Sdmx.getDatesBoundaries(state([undefined, '2000-01-01T00:00:00.000Z'], [false, false]))).toEqual([
        new Date('1970-01-01T00:00:00'),
        new Date('1999-01-01T00:00:00'),
      ]);
      expect(Sdmx.getDatesBoundaries(state(['1990-01-01T00:00:00.000Z', undefined], [false, false]))).toEqual([
        new Date('1991-01-01T00:00:00'),
        new Date('2050-01-01T00:00:00'),
      ]);
      expect(Sdmx.getDatesBoundaries(state([undefined, undefined], [false, false]))).toEqual([
        new Date('1970-01-01T00:00:00.000'),
        new Date('2050-01-01T00:00:00.000'),
      ]);
    });
  });
  describe('getDimensionsWithDataQuerySelection', () => {
    test('use cases', () => {
      const state = (dimensions = [], contentConstraints, dataquery) => ({
        sdmx: { dimensions, contentConstraints },
        router: {
          params: {
            dataquery,
          },
        },
      });
      expect(Sdmx.getDimensionsWithDataQuerySelection(state())).toEqual([]);
      expect(
        Sdmx.getDimensionsWithDataQuerySelection(state([{ id: 1, display: true, values: [{}, {}] }, { id: 2 }])),
      ).toEqual([{ id: 1, display: true, values: [{}, {}] }]);
      expect(Sdmx.getDimensionsWithDataQuerySelection(state([{}, {}], {}, '.A'))).toEqual([]);
    });
  });
});
