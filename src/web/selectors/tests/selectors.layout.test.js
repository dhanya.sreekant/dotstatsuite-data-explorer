import * as S from '../.';

describe('selectors | for the layout', () => {
  const data = {
    structure: {
      dimensions: {
        observation: [
          { id: 'test1', values: [{}, {}] },
          { id: 'TIME_PERIOD', values: [{}, {}] },
          { id: 'test2', values: [{}] },
        ],
      },
    },
  };
  describe('getTableConfigDimensions', () => {
    test('DETableConfig need special dimensions format', () => {
      const expected = {
        TIME_PERIOD: {
          id: 'TIME_PERIOD',
          isTimePeriod: true,
          name: undefined,
          value: 'TIME_PERIOD',
          values: [
            {
              id: 'TIME_PERIOD-0',
              label: 'Xxxx',
            },
            {
              id: 'TIME_PERIOD-1',
              label: 'Xxxx',
            },
          ],
        },
        test1: {
          id: 'test1',
          isTimePeriod: false,
          name: undefined,
          value: 'test1',
          values: [
            {
              id: 'test1-0',
              label: 'Xxxx',
            },
            {
              id: 'test1-1',
              label: 'Xxxx',
            },
          ],
        },
        test2: {
          id: 'test2',
          isHidden: true,
          value: 'test2',
        },
      };
      expect(
        S.getTableConfigDimensions()({ sdmx: { data }, vis: { dimensionGetter: 'label' } }),
      ).toEqual(expected);
    });
  });
  describe('getVisTableConfigLayout ', () => {
    test('get default layout, starting layout is empty', () => {
      const expected = { rows: ['test1'], header: ['TIME_PERIOD'], sections: ['test2'] };
      expect(S.getVisTableConfigLayout()({ sdmx: { data } })).toEqual(expected);
    });
    test('keep user change', () => {
      const layout = { rows: ['TIME_PERIOD'], header: ['test1'], sections: ['test2'] };
      const expected = layout;
      expect(S.getVisTableConfigLayout()({ sdmx: { data }, vis: { layout } })).toEqual(expected);
    });
    test('row not possible to be empty', () => {
      const layout = { rows: [], header: ['test2'], sections: ['TIME_PERIOD', 'test1'] };
      const expected = { rows: ['TIME_PERIOD'], header: ['test2'], sections: ['test1'] };
      expect(S.getVisTableConfigLayout()({ sdmx: { data }, vis: { layout } })).toEqual(expected);
    });
  });
  describe('getVisTableLayout  ', () => {
    test('remove one values in the layout', () => {
      const expected = { rows: ['test1'], header: ['TIME_PERIOD'], sections: [] };
      expect(S.getVisTableLayout()({ sdmx: { data } })).toEqual(expected);
    });
  });
});
