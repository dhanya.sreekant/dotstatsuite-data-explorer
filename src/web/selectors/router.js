import { createSelector } from 'reselect';
import * as R from 'ramda';
import { locales, getDatasource } from '../lib/settings';

//------------------------------------------------------------------------------------------------#0
const getRouter = R.prop('router');

//------------------------------------------------------------------------------------------------#1
export const getParams = createSelector(getRouter, R.propOr({}, 'params'));

export const getLocation = createSelector(getRouter, R.propOr({}, 'location'));

//------------------------------------------------------------------------------------------------#2
export const getTerm = createSelector(getParams, R.prop('term'));

export const getHasAccessibility = createSelector(getParams, R.pipe(R.prop('hasAccessibility'), Boolean));

export const getConstraints = createSelector(getParams, R.propOr({}, 'constraints'));

export const getFacet = createSelector(getParams, R.prop('facet'));

export const getFilter = createSelector(getParams, R.prop('filter'));

export const getLocale = createSelector(getParams, R.prop('locale'));

export const getDataflow = createSelector(getParams, R.prop('dataflow'));

export const getDataquery = createSelector(getParams, R.prop('dataquery'));

export const getViewer = createSelector(getParams, R.propOr('table', 'viewer'));

export const getStart = createSelector(getParams, R.prop('start'));

export const getFrequency = createSelector(getParams, R.prop('frequency'));

export const getIsRtl = createSelector(getLocale, localeId =>
  R.pipe(R.prop(localeId), R.prop('isRtl'))(locales),
);

export const getHasLastNObservations = createSelector(getDataflow, ({ datasourceId }) =>
  R.propOr(false, 'hasLastNObservations', getDatasource(datasourceId)),
);

export const getLastNObservations = createSelector(
  getParams,
  getHasLastNObservations,
  ({ lastNObservations }, hasLastNObservations) =>
    hasLastNObservations ? lastNObservations : null,
);

export const getMap = createSelector(getParams, R.prop('map'));

export const getDataAvailability = createSelector(
  getParams,
  R.pipe(R.propOr('on', 'dataAvailability'), R.equals('off')),
);
