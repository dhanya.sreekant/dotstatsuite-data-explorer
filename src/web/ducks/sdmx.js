import * as R from 'ramda';

//---------------------------------------------------------------------------------------------model
export const model = () => ({
  dimensions: [],
  data: undefined,
  layout: {},
  frequencyArtefact: {},
  externalResources: [],
  availableFrequencies: {
    ids: [],
    labels: {},
  },
  hasContentConstraints: false,
});

//-----------------------------------------------------------------------------------------constants
export const HANDLE_STRUCTURE = '@@sdmx/HANDLE_STRUCTURE';
export const REQUEST_STRUCTURE = '@@sdmx/REQUEST_STRUCTURE';
export const PARSE_STRUCTURE = '@@sdmx/PARSE_STRUCTURE';
export const HANDLE_DATA = '@@sdmx/HANDLE_DATA';
export const REQUEST_DATA = '@@sdmx/REQUEST_DATA';
export const PARSE_DATA = '@@sdmx/PARSE_DATA';
export const FLUSH_DATA = '@@sdmx/FLUSH_DATA';

export const REQUEST_DATAFILE = 'REQUEST_DATAFILE';
export const requestDataFile = ({ isDownloadAllData, dataflow }) => ({
  type: REQUEST_DATAFILE,
  payload: { isDownloadAllData, dataflow },
});

//-------------------------------------------------------------------------------------------reducer
export default (state = model(), action = {}) => {
  switch (action.type) {
    case FLUSH_DATA:
      return R.set(R.lensProp('data'), undefined, state);
    case HANDLE_STRUCTURE:
      return R.pipe(
        R.set(R.lensProp('hasContentConstraints'), R.path(['structure', 'hasContentConstraints'], action)),
        R.set(R.lensProp('externalResources'), R.path(['structure', 'externalResources'], action)),
        R.set(R.lensProp('frequencyArtefact'), R.path(['structure', 'frequencyArtefact'], action)),
        R.set(R.lensProp('dimensions'), R.path(['structure', 'dimensions'], action)),
        R.set(R.lensProp('timePeriod'), R.path(['structure', 'timePeriod'], action)),
        R.set(R.lensProp('name'), R.path(['structure', 'title'], action)),
        R.set(
          R.lensProp('contentConstraints'),
          R.path(['structure', 'contentConstraints'], action),
        ),
        R.set(
          R.lensProp('timePeriodBoundaries'),
          R.path(['structure', 'timePeriodBoundaries'], action),
        ),
        R.set(
          R.lensProp('timePeriodIncludingBoundaries'),
          R.path(['structure', 'timePeriodIncludingBoundaries'], action),
        ),
        R.dissoc('range')
      )(state);
    case HANDLE_DATA:
      return R.pipe(
        R.set(R.lensProp('data'), R.prop('data', action)),
        R.set(R.lensProp('range'), R.prop('range', action)),
      )(state);
    default:
      return state;
  }
};
