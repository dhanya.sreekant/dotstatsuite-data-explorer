import * as R from 'ramda';
import md5 from 'md5';
import * as SDMXJS from '@sis-cc/dotstatsuite-sdmxjs';
import { RESET_SEARCH, resetSearch } from './search';
import { HANDLE_STRUCTURE } from './sdmx';
import { getLocaleId } from '../lib/settings';
import { setLocale } from '../i18n';
import { history } from '../router';
import { getDataquery, getLocation, getFrequency } from '../selectors/router';
import {
  getContentConstrainedDimensions,
  getPeriod,
  getFrequencyArtefact,
  getFrequencyArtefactContentConstrained,
  getContentConstraints,
  getDimensions,
} from '../selectors/sdmx';
import { getSdmxPeriod, changeFrequency, getFrequencies } from '../lib/sdmx/frequency';
import { getSelectedIdsIndexed, setSelectedDimensionsValues } from '../lib/sdmx';
import { START_PERIOD, END_PERIOD, LASTN } from '../utils/used-filter';
import { PANEL_PERIOD } from '../utils/constants';

//---------------------------------------------------------------------------------------------model
export const model = () => ({
  location: {},
  params: {
    term: undefined, // term: '' (search)
    constraints: {}, // list of constraints: {hash: {facetId, constraintId}} (search)
    facet: undefined, // id of the expanded facet: '' (search)
    start: 0, // first index of the dataflow to returned (search)
    locale: getLocaleId(), // id of the current locale
    dataflow: undefined, // dataflow identifiers: {datasourceId, dataflowId, agencyId, version} (sdmx)
    filter: undefined, // id of the expanded filter: '' (vis)
    dataquery: undefined, // dataquery: '' (sdmx)
    frequency: undefined, // frequency for dataflow without frequency only
    period: undefined, // period: [start, end] (sdmx)
    lastNObservations: undefined, // query parameter: '(integer)' (sdmx)
    viewer: undefined, // id of the vis component to display: '' (vis)
    map: undefined,
    dataAvailability: 'on',
    hasAccessibility: false,
  },
});

//-----------------------------------------------------------------------------------------constants
export const CHANGE_LOCATION = '@@router/CHANGE_LOCATION';
export const CHANGE_LOCALE = '@@router/CHANGE_LOCALE';
export const CHANGE_HAS_ACCESSIBILITY = '@@router/CHANGE_HAS_ACCESSIBILITY';
export const CHANGE_TERM = '@@router/search/CHANGE_TERM';
export const CHANGE_CONSTRAINTS = '@@router/search/CHANGE_CONSTRAINTS';
export const CHANGE_FACET = '@@router/search/CHANGE_FACET';
export const CHANGE_START = '@@router/search/CHANGE_START';
export const CHANGE_DATAFLOW = '@@router/sdmx/CHANGE_DATAFLOW';
export const RESET_DATAFLOW = '@@router/sdmx/RESET_DATAFLOW';
export const CHANGE_DATAQUERY = '@@router/sdmx/CHANGE_DATAQUERY';
export const CHANGE_LAST_N_OBS = '@@router/sdmx/CHANGE_LAST_N_OBS';
export const CHANGE_FILTER = '@@router/vis/CHANGE_FILTER';
export const DEFAULT_FILTER = '@@router/vis/DEFAULT_FILTER';
export const CHANGE_VIEWER = '@@router/vis/CHANGE_VIEWER';
export const CHANGE_FREQUENCY_PERIOD = '@@router/vis/CHANGE_FREQUENCY_PERIOD';
export const APPLY_DATA_AVAILABILITY = '@@router/vis/APPLY_DATA_AVAILABILITY';
//------------------------------------------------------------------------------------------creators
export const changeLocation = (location, params) => (dispatch, getState) => {
  const defaultAction = { type: CHANGE_LOCATION, payload: { location, params } };

  const prevLocation = getLocation(getState());
  const isFromAnotherPath = prevLocation =>
    R.pipe(
      R.prop('pathname'),
      R.both(R.equals('/'), R.complement(R.equals)(R.prop('pathname', prevLocation))),
    );
  const hasSearchChanged = prevLocation => location =>
    R.both(
      R.pipe(R.pluck('search'), R.complement(R.equals)),
      R.all(R.pipe(R.prop('pathname'), R.equals('/'))),
    )([prevLocation, location]);
  const shouldRequestSearch = R.either(
    isFromAnotherPath(prevLocation),
    hasSearchChanged(prevLocation),
  )(location);

  if (shouldRequestSearch) return dispatch(R.assoc('request', 'getSearch', defaultAction));

  if (R.not(R.propEq('pathname', '/vis', location))) return dispatch(defaultAction);
  dispatch(
    R.assoc(
      'request',
      R.pipe(R.prop('pathname'), R.either(R.isNil, R.equals('/')))(getLocation(getState()))
        ? 'getStructure'
        : 'getData',
    )(defaultAction),
  );
};
export const changeTerm = ({ term } = {}) => ({
  type: CHANGE_TERM,
  payload: { term },
  pushHistory: '/',
  request: 'getSearch',
});
export const changeConstraints = (facetId, constraintId) => ({
  type: CHANGE_CONSTRAINTS,
  payload: { facetId, constraintId },
  pushHistory: '/',
  request: 'getSearch',
});
export const changeFacet = facetId => ({
  type: CHANGE_FACET,
  payload: { facetId },
  pushHistory: '/',
});
export const changeStart = start => ({
  type: CHANGE_START,
  payload: { start },
  pushHistory: '/',
  request: 'getSearch',
});
export const changeHasAccessibility = hasAccessibility => (dispatch, getState) => {
  const location = getLocation(getState());
  return dispatch({
    type: CHANGE_HAS_ACCESSIBILITY,
    payload: { hasAccessibility },
    pushHistory: R.propOr('/', 'pathname')(location),
  });
}

export const changeLocale = localeId => {
  setLocale(localeId);
  const pushHistory = R.path(['location', 'pathname'], history);
  return R.omit([R.equals('/vis')(pushHistory) ? null : 'request'], {
    type: CHANGE_LOCALE,
    payload: { localeId },
    pushHistory,
    request: 'getStructure',
  });
};

export const changeDataflow = dataflow => ({
  type: CHANGE_DATAFLOW,
  payload: { dataflow },
  pushHistory: '/vis',
});
export const resetDataflow = () => ({
  type: RESET_DATAFLOW,
  pushHistory: '/',
});
export const resetDataflowAndSearch = () => dispatch => {
  dispatch(resetSearch());
  dispatch(resetDataflow());
};
export const changeFilter = filterId => ({
  type: CHANGE_FILTER,
  payload: { filterId },
  pushHistory: '/vis',
});
export const changeDataquery = (filterId, valueId) => (dispatch, getState) => {
  const dimensions = getContentConstrainedDimensions(getState());
  const dataquery = getDataquery(getState());
  dispatch({
    type: CHANGE_DATAQUERY,
    payload: {
      dataquery: SDMXJS.updateDataquery(dimensions, dataquery, filterId, valueId),
    },
    pushHistory: '/vis',
    request: 'getData',
  });
};
export const changeViewer = (id, option = {}) => ({
  type: CHANGE_VIEWER,
  payload: {
    viewerId: R.has('map', option) ? 'ChoroplethChart' : id,
    map: R.prop('map', option),
  },
  pushHistory: '/vis',
});

export const changeLastNObservations = value => ({
  type: CHANGE_LAST_N_OBS,
  payload: { value },
  pushHistory: '/vis',
  request: 'getData',
});
export const changeFrequencyPeriod = ({ valueId, dates } = {}) => (dispatch, getState) => {
  const period = R.or(R.isEmpty(dates), R.isNil(dates)) ? [undefined, undefined] : dates;
  const filterId = R.prop('id')(getFrequencyArtefactContentConstrained(getState()));
  const action = {
    type: CHANGE_FREQUENCY_PERIOD,
    payload: {
      frequency: valueId,
      period: R.map(getSdmxPeriod(valueId))(period),
    },
  };
  // need request getData, and pushHistory: /vis, actually trigger by changeDataquery
  if (R.not(R.isNil(filterId))) {
    dispatch(action);
    dispatch(changeDataquery(filterId, valueId));
    return;
  }
  dispatch(R.pipe(R.assoc('request', 'getData'), R.assoc('pushHistory', '/vis'))(action));
};
export const deleteSpecialFilter = (_, id) => (dispatch, getState) => {
  // special is lastNObservations
  if (R.equals(LASTN, id)) return dispatch(changeLastNObservations());

  // special is start/end -> period
  if (R.either(R.equals(START_PERIOD), R.equals(END_PERIOD))(id)) {
    const payloadPeriod = period => {
      if (R.equals(START_PERIOD, id)) return [undefined, R.last(period)];
      if (R.equals(END_PERIOD, id)) return [R.head(period), undefined];
    };
    return dispatch({
      type: CHANGE_FREQUENCY_PERIOD,
      payload: { period: payloadPeriod(getPeriod(getState())) },
      pushHistory: '/vis',
      request: 'getData',
    });
  }

  // all special
  if (R.isNil(id)) {
    dispatch({ type: CHANGE_LAST_N_OBS }); // without getData
    return dispatch({
      type: CHANGE_FREQUENCY_PERIOD,
      payload: { period: [undefined, undefined] },
      pushHistory: '/vis',
      request: 'getData',
    });
  }
};
export const deleteAllFilters = () => (dispatch, getState) => {
  dispatch({ type: CHANGE_LAST_N_OBS }); // without getData
  dispatch({
    type: CHANGE_FREQUENCY_PERIOD,
    payload: { period: [undefined, undefined], frequency: getFrequency(getState()) },
  }); // without getData
  return dispatch(changeDataquery());
};

export const applyDataAvailability = isChecked => (dispatch, getState) => {
  dispatch({
    type: APPLY_DATA_AVAILABILITY,
    payload: { dataAvailability: isChecked ? 'off' : 'on' },
    pushHistory: '/vis',
  });
  const contentConstraints = getContentConstraints(getState());
  if (R.isNil(contentConstraints)) return;
  const currentDataquery = getDataquery(getState());
  const dimensions = isChecked 
    ? getDimensions(getState())
    : getContentConstrainedDimensions(getState());
  const dimensionsWithSelectedValues = setSelectedDimensionsValues(currentDataquery, dimensions);
  const frequencyArtefact = isChecked 
    ? getFrequencyArtefact(getState())
    : getFrequencyArtefactContentConstrained(getState());
  const frequencyId = R.prop('id')(frequencyArtefact);
  const availableFrequencies = getFrequencies(frequencyArtefact);
  const frequency = changeFrequency(getFrequency(getState()))(availableFrequencies);
  const selection = R.assoc(frequencyId, frequency, getSelectedIdsIndexed(dimensionsWithSelectedValues)) 
  const dataquery = SDMXJS.getDataquery(dimensions, selection)
  
  if (R.equals(currentDataquery, dataquery)) return;
  dispatch({
    type: CHANGE_FREQUENCY_PERIOD,
    payload: { frequency },
  });
  dispatch({
    type: CHANGE_DATAQUERY,
    payload: { dataquery },
    pushHistory: '/vis',
    request: 'getData',
  });
};
//-------------------------------------------------------------------------------------------reducer
export default (state = model(), action = {}) => {
  switch (action.type) {
    case CHANGE_LOCATION:
      return R.pipe(
        R.set(R.lensProp('location'), R.path(['payload', 'location'], action)),
        R.set(
          R.lensProp('params'),
          R.evolve(
            {
              constraints: R.reduce(
                (memo, [facetId, constraintId]) =>
                  R.assoc(md5(`${facetId}${constraintId}`), { facetId, constraintId }, memo),
                {},
              ),
            },
            R.pipe(R.path(['payload', 'params']), R.mergeRight({ locale: getLocaleId() }))(action), // apply default, could *also* be useful for evolve transformations
          ),
        ),
      )(state);
    case CHANGE_FREQUENCY_PERIOD:
      return R.pipe(R.over(R.lensProp('params'), R.flip(R.mergeRight)(R.prop('payload', action))))(
        state,
      );
    case CHANGE_TERM:
      return R.pipe(
        R.over(R.lensProp('params'), R.flip(R.mergeRight)(R.prop('payload', action))),
        R.set(R.lensPath(['params', 'start']), 0),
      )(state);
    case CHANGE_DATAQUERY:
      return R.over(R.lensProp('params'), R.flip(R.mergeRight)(R.prop('payload', action)), state);
    case CHANGE_FACET:
      return R.over(
        R.lensPath(['params', 'facet']),
        facet =>
          R.ifElse(
            R.equals(facet),
            R.always(undefined),
            R.identity,
          )(R.path(['payload', 'facetId'], action)),
        state,
      );
    case CHANGE_START:
      return R.set(R.lensPath(['params', 'start']), R.path(['payload', 'start'], action), state);
    case CHANGE_FILTER:
      return R.over(
        R.lensPath(['params', 'filter']),
        filterId => {
          const nextFilterId = R.path(['payload', 'filterId'], action);
          if (R.all(R.isNil)([filterId, nextFilterId])) return PANEL_PERIOD;
          if (R.equals(filterId, nextFilterId)) return undefined;
          return nextFilterId;
        },
        state,
      );
    case CHANGE_VIEWER:
      return R.pipe(
        R.set(R.lensPath(['params', 'viewer']), R.path(['payload', 'viewerId'], action)),
        R.set(R.lensPath(['params', 'map']), R.path(['payload', 'map'], action)),
      )(state);
    case CHANGE_CONSTRAINTS:
      var { facetId, constraintId } = action.payload;

      if (R.isNil(facetId)) {
        // undefined facetId <=> no constraint
        return R.pipe(
          R.set(R.lensPath(['params', 'facet']), null),
          R.set(R.lensPath(['params', 'constraints']), {}),
          R.set(R.lensPath(['params', 'start']), 0),
        )(state);
      }

      if (R.isNil(constraintId)) {
        // undefined constraintId <=> remove facet from constraints
        return R.pipe(
          R.set(R.lensPath(['params', 'facet']), null),
          R.over(R.lensPath(['params', 'constraints']), R.reject(R.propEq('facetId', facetId))),
          R.set(R.lensPath(['params', 'start']), 0),
        )(state);
      }

      var hash = md5(`${facetId}${constraintId}`);
      return R.pipe(
        R.set(R.lensPath(['params', 'facet']), facetId),
        R.ifElse(
          R.hasPath(['params', 'constraints', hash]),
          R.over(R.lensPath(['params', 'constraints']), R.dissoc(hash)),
          R.over(R.lensPath(['params', 'constraints']), R.assoc(hash, { facetId, constraintId })),
        ),
        R.set(R.lensPath(['params', 'start']), 0),
      )(state);
    case RESET_SEARCH:
      return R.over(R.lensProp('params'), R.pick(['locale', 'tenant', 'hasAccessibility']), state);
    case CHANGE_LOCALE:
      return R.pipe(
        R.set(
          R.lensPath(['params', 'locale']),
          getLocaleId(R.path(['payload', 'localeId'], action)),
        ),
        R.over(R.lensProp('params'), R.omit(['term', 'constraints', 'facet'])),
        R.set(R.lensPath(['params', 'start']), 0),
      )(state);
    case CHANGE_DATAFLOW:
      return R.pipe(
        R.over(R.lensProp('params'), R.omit(['viewer'])),
        R.over(R.lensProp('params'), R.flip(R.merge)(R.prop('payload', action))),
      )(state);
    case RESET_DATAFLOW:
      return R.over(
        R.lensProp('params'),
        R.omit([
          'dataflow',
          'dataquery',
          'filter',
          'lastNObservations',
          'period',
          'viewer',
          'frequency',
          'dataAvailability',
        ]),
        state,
      );
    case HANDLE_STRUCTURE:
      return R.over(
        R.lensProp('params'),
        R.mergeRight(R.pathOr({}, ['structure', 'params'], action)),
      )(state);
    case CHANGE_LAST_N_OBS:
      return R.set(
        R.lensPath(['params', 'lastNObservations']),
        R.path(['payload', 'value'], action),
      )(state);
    case APPLY_DATA_AVAILABILITY:
      return R.set(
        R.lensPath(['params', 'dataAvailability']),
        R.path(['payload', 'dataAvailability'], action),
        state,
      );
    case CHANGE_HAS_ACCESSIBILITY: 
      return R.set(
        R.lensPath(['params', 'hasAccessibility']),
        R.path(['payload', 'hasAccessibility'], action),
        state,
      );
    default:
      return state;
  }
};
