import * as R from 'ramda';
import { CHANGE_LOCALE } from './router';
import { getTerm, getConstraints, getLocale, getStart } from '../selectors/router';
import { getConfig, getHasNoSearchParams, getRows } from '../selectors/search';
import searchApi from '../api/search';
import { setPending, pushLog, LOG_ERROR } from './app';
import { defaultSearchRows, sdmx, search, indexedDatasources, valueIcons } from '../lib/settings';

const isDev = process.env.NODE_ENV === 'development';

//---------------------------------------------------------------------------------------------model
export const model = () => ({
  dataflows: [],
  numFound: undefined, // API count relative to a search
  facets: [],
  config: undefined,
  rows: defaultSearchRows, // number of dataflows per page (search)
});

//-----------------------------------------------------------------------------------------constants
export const HANDLE_CONFIG = '@@search/HANDLE_CONFIG';
export const HANDLE_SEARCH = '@@search/HANDLE_SEARCH';
export const RESET_SEARCH = '@@search/RESET_SEARCH';

//------------------------------------------------------------------------------------------creators
export const resetSearch = () => ({
  type: RESET_SEARCH,
  pushHistory: '/',
});

//--------------------------------------------------------------------------------------thunks (api)
const request = (dispatch, ctx) => {
  const { method } = ctx;
  const pendingId = ctx.pendingId || method;

  // eslint-disable-next-line no-console
  if (isDev) console.info(`request: ${pendingId}`);

  dispatch(setPending(pendingId, true));
  return searchApi(ctx)
    .then(res => {
      dispatch(setPending(pendingId));
      return res;
    })
    .catch(error => {
      const log = error.response
        ? { method, errorCode: error.response.data.errorCode, statusCode: error.response.status }
        : { method, error };

      dispatch(setPending(pendingId));
      dispatch(pushLog({ type: LOG_ERROR, payload: { log } }));

      // required to break the promised chain
      throw error;
    });
};

export const requestConfig = () => (dispatch, getState) => {
  const locale = getLocale(getState());
  if (R.equals(R.prop('locale', getConfig(getState())), locale)) return Promise.resolve();

  const requestArgs = { lang: locale, facets: { datasourceId: indexedDatasources } };
  const parserArgs = {
    facetIds: R.prop('homeFacetIds', search),
    config: { valueIcons },
  };
  return request(dispatch, { method: 'getConfig', requestArgs, parserArgs }).then(config =>
    dispatch({ type: HANDLE_CONFIG, config }),
  );
};

export const requestSearch = () => (dispatch, getState) => {
  if (getHasNoSearchParams(getState())) return Promise.resolve();
  requestConfig()(dispatch, getState).then(() => {
    const requestArgs = {
      lang: getLocale(getState()),
      search: getTerm(getState()),
      facets: R.pipe(
        R.ifElse(
          R.isEmpty,
          R.identity,
          R.reduceBy((acc, { constraintId }) => acc.concat(constraintId), [], R.prop('facetId')),
        ),
        R.ifElse(
          R.has('datasourceId'),
          R.identity,
          R.assoc('datasourceId', R.propOr([], 'datasourceIds', sdmx)),
        ),
      )(R.values(getConstraints(getState()))),
      rows: getRows(getState()),
      start: getStart(getState()),
    };
    const parserArgs = {
      config: { valueIcons },
      constraints: getConstraints(getState()),
    };

    return request(dispatch, { method: 'getSearch', requestArgs, parserArgs }).then(search =>
      dispatch({ type: HANDLE_SEARCH, search }),
    );
  });
};

//-------------------------------------------------------------------------------------------reducer
export default (state = model(), action = {}) => {
  switch (action.type) {
    case HANDLE_CONFIG:
      return R.set(R.lensProp('config'), R.prop('config', action), state);
    case HANDLE_SEARCH:
      return R.pipe(
        R.prop('search'),
        R.pick(['dataflows', 'facets', 'numFound']),
        R.mergeRight(state),
      )(action);
    case RESET_SEARCH:
      return {
        ...R.pick(['config'], state),
        rows: defaultSearchRows,
      };
    case CHANGE_LOCALE:
      return model();
    default:
      return state;
  }
};
