import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, withProps, pure, branch, renderNothing } from 'recompose';
import * as R from 'ramda';
import { injectIntl } from 'react-intl';
import {
  Chips,
  ScopeList,
  ExpansionPanel,
  PeriodPicker,
  InputNumber,
  Tag,
  Spotlight,
  spotlightScopeListEngine,
} from '@sis-cc/dotstatsuite-visions';
import { rules } from '@sis-cc/dotstatsuite-components';
import { PanelContentConstraints, ActiveContentConstraints } from './vis-side/content-constraints';
import {
  changeFilter,
  changeDataquery,
  deleteSpecialFilter,
  deleteAllFilters,
  changeFrequencyPeriod,
  changeLastNObservations,
} from '../ducks/router';
import { PANEL_PERIOD, PANEL_USED_FILTERS } from '../utils/constants';
import {
  getFilter,
  getLastNObservations,
  getHasLastNObservations,
  getLocale,
  getFrequency,
  getHasAccessibility,
} from '../selectors/router';
import {
  getDimensionsWithDataQuerySelection,
  getSelection,
  getFrequencyArtefactContentConstrained,
  getPeriod,
  getTimePeriod,
  getDatesBoundaries,
  getAvailableFrequencies,
  getNotApplyDataAvailabilty
} from '../selectors/sdmx';
import { getIntervalPeriod, getDateFromSdmxPeriod } from '../lib/sdmx/frequency.js';
import { countNumberOf } from '../utils';
import {
  START_PERIOD,
  END_PERIOD,
  LASTN,
  addI18nLabels,
  getUsedFilterPeriod,
  getUsedFilterFrequency,
} from '../utils/used-filter';
import { locales } from '../lib/settings';

const withFilterFrequency = withProps(({ frequency, frequencyArtefact, availableFrequencies }) => ({
  frequencyFilter: getUsedFilterFrequency(frequency, availableFrequencies)(frequencyArtefact),
}));

const withFilterPeriod = withProps(({ intl, period, lastN, locale, timePeriod }) => ({
  periodFilter: getUsedFilterPeriod(
    period,
    lastN,
    rules.getTimePeriodLabel(locale, R.path([locale, 'timeFormat'])(locales)),
    timePeriod,
  ),
  periodLabels: {
    [START_PERIOD]: intl.formatMessage({ id: 'de.period.start' }),
    [END_PERIOD]: intl.formatMessage({ id: 'de.period.end' }),
    [LASTN]: [
      intl.formatMessage({ id: 'de.period.last' }),
      intl.formatMessage({ id: 'de.period.periods' }),
    ],
  },
}));

const withDatesFromSdmxPeriod = withProps(({ period, frequency }) => ({
  dates: getDateFromSdmxPeriod(frequency, period),
}));

//--------------------------------------------------------------------------------------------------
export const UsedFilters = compose(
  connect(
    createStructuredSelector({
      items: getSelection,
    }),
    { onDelete: changeDataquery },
  ),
  pure,
)(Chips);

export const SpecialUsedFilters = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      frequency: getFrequency,
      frequencyArtefact: getFrequencyArtefactContentConstrained,
      period: getPeriod,
      lastN: getLastNObservations,
      locale: getLocale,
      timePeriod: getTimePeriod,
    }),
    {
      onDeleteAll: deleteAllFilters,
      onDelete: deleteSpecialFilter,
    },
  ),
  withFilterFrequency,
  withFilterPeriod,
  withProps(({ intl, frequencyFilter = [], periodFilter = [], periodLabels }) => ({
    items: R.concat(frequencyFilter, addI18nLabels(periodLabels)(periodFilter)),
    clearAllLabel: intl.formatMessage({ id: 'vx.filters.current.clear' }),
  })),
  pure,
)(Chips);

export const FiltersCurrent = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      activePanelId: getFilter,
      items1: getSelection,
      frequency: getFrequency,
      frequencyArtefact: getFrequencyArtefactContentConstrained,
      period: getPeriod,
      lastN: getLastNObservations,
      locale: getLocale,
      timePeriod: getTimePeriod,
    }),
    { onChangeActivePanel: changeFilter },
  ),
  withFilterFrequency,
  withFilterPeriod,
  withProps(({ intl, items1 = [], frequencyFilter = [], periodFilter = [], activePanelId }) => ({
    isOpen: R.equals(PANEL_USED_FILTERS, activePanelId),
    id: PANEL_USED_FILTERS,
    label: intl.formatMessage({ id: 'vx.filters.current.title' }),
    tag: <Tag>{countNumberOf(R.concat(items1, R.concat(frequencyFilter, periodFilter)))}</Tag>,
  })),
)(ExpansionPanel);

//--------------------------------------------------------------------------------------------------
const Filter = pure(ScopeList);
export const Filters = compose(
  injectIntl,
    connect(
      createStructuredSelector({ 
        filters: getDimensionsWithDataQuerySelection, 
        activePanelId: getFilter,
        notApplyDataAvailabilty: getNotApplyDataAvailabilty,
        accessibility: getHasAccessibility
      }),
      { changeSelection: changeDataquery, onChangeActivePanel: changeFilter }
    ),
  withProps(({ intl }) => ({
    labels: {
      navigateNext: intl.formatMessage({ id: 'wcag.scopelist.navigate.next' }),
      navigateBefore: intl.formatMessage({ id: 'wcag.scopelist.navigate.before' }),
    },
    topElementProps: {
      fullWidth: true,
      hasClearAll: true,
      mainPlaceholder: intl.formatMessage({ id: 'vx.spotlight.placeholder.primary' }),
      secondaryPlaceholder: intl.formatMessage({ id: 'vx.spotlight.placeholder.secondary' }),
      defaultSpotlight: {
        engine: spotlightScopeListEngine,
        placeholder: intl.formatMessage({ id: 'vx.spotlight.placeholder' }),
        fields: {
          label: {
            id: 'label',
            accessor: R.pipe(R.propOr(null, 'label'), R.ifElse(R.isNil, R.always(''), R.identity)),
            isSelected: true,
          },
        },
      },
    },
    topElementComponent: values => (R.gte(R.length(values), 8) ? Spotlight : null),
  })),
  pure,
)(({ topElementComponent, filters, notApplyDataAvailabilty, ...parentProps }) =>
  R.map(
    ({ id, label, values = [] }) => (
      <Filter
        {...parentProps}
        limitDisplay={1}
        displayAccessor={notApplyDataAvailabilty ? undefined : R.prop('hasData')}
        id={id}
        key={id}
        label={label}
        items={values}
        TopElementComponent={topElementComponent(values)}
      />
    ),
    filters,
  ),
);

//--------------------------------------------------------------------------------------------------
export const Period = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      period: getPeriod,
      frequency: getFrequency,
      availableFrequencies: getAvailableFrequencies,
      boundaries: getDatesBoundaries,
    }),
    { changeFrequencyPeriod },
  ),
  withDatesFromSdmxPeriod,
  withProps(
    ({
      availableFrequencies,
      frequency,
      dates,
      changeFrequencyPeriod,
      intl,
      boundaries,
    }) => ({
      defaultFrequency: frequency,
      availableFrequencies: R.keys(availableFrequencies),
      boundaries,
      period: dates,
      labels: R.mergeRight({
        year: intl.formatMessage({ id: 'de.period.year' }),
        semester: intl.formatMessage({ id: 'de.period.semester' }),
        quarter: intl.formatMessage({ id: 'de.period.quarter' }),
        month: intl.formatMessage({ id: 'de.period.month' }),
        week: intl.formatMessage({ id: 'de.period.week' }),
        hour: intl.formatMessage({ id: 'de.period.hour' }),
        minute: intl.formatMessage({ id: 'de.period.minute' }),
        full_hour: intl.formatMessage({ id: 'de.period.full_hour' }),
        day: intl.formatMessage({ id: 'de.period.day' }),
        end: intl.formatMessage({ id: 'de.period.end' }),
        start: intl.formatMessage({ id: 'de.period.start' }),
      })(availableFrequencies),
      changeFrequency: frequency =>
        changeFrequencyPeriod({
          valueId: frequency,
          dates,
        }),
      changePeriod: dates => changeFrequencyPeriod({ valueId: frequency, dates }),
    }),
  ),
  pure,
)(PeriodPicker);

export const LastNPeriod = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      value: getLastNObservations,
      hasLastNObservations: getHasLastNObservations,
    }),
    { onChange: changeLastNObservations },
  ),
  withProps(({ intl }) => ({
    beforeLabel: intl.formatMessage({ id: 'de.period.last' }),
    afterLabel: intl.formatMessage({ id: 'de.period.periods' }),
    popperLabel: intl.formatMessage({ id: 'de.period.helpLastNPeriods' }),
  })),
  branch(({ hasLastNObservations }) => R.not(hasLastNObservations), renderNothing),
)(InputNumber);

export const FilterPeriod = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      activePanelId: getFilter,
      period: getPeriod,
      frequency: getFrequency,
      availableFrequencies: getAvailableFrequencies,
      datesBoundaries: getDatesBoundaries,
    }),
    { changeFilter, onChangeActivePanel: changeFilter },
  ),
  withProps(({ intl, activePanelId, period, frequency, availableFrequencies, datesBoundaries }) => {
    const dates = getDateFromSdmxPeriod(frequency, period);
    return {
      isBlank: R.all(R.isNil)(datesBoundaries),
      label: R.or(
        R.isEmpty(availableFrequencies),
        R.pipe(R.keys, R.length, R.equals(1))(availableFrequencies),
      )
        ? intl.formatMessage({ id: 'de.filter.period.title.last' })
        : R.join(' & ')([
            intl.formatMessage({ id: 'de.filter.period.title.head' }),
            intl.formatMessage({ id: 'de.filter.period.title.last' }),
          ]),
      overflow: true,
      id: PANEL_PERIOD,
      isOpen: R.equals(PANEL_PERIOD, activePanelId),
      tag: <Tag>{R.join(' / ')(getIntervalPeriod(datesBoundaries)(frequency, dates))}</Tag>,
    };
  }),
)(ExpansionPanel);

//----------------------------------------------------------------------------------------------Side
const Side = ({ isNarrow, isRtl }) => (
  <Fragment>
    <FiltersCurrent isNarrow={isNarrow} isRtl={isRtl}>
      <UsedFilters />
      <SpecialUsedFilters />
    </FiltersCurrent>
    <FilterPeriod isNarrow={isNarrow} isRtl={isRtl}>
      <Period />
      <LastNPeriod />
    </FilterPeriod>
    <Filters isNarrow={isNarrow} isRtl={isRtl} />
    <PanelContentConstraints>
      <ActiveContentConstraints />
    </PanelContentConstraints>
  </Fragment>
);

Side.propTypes = {
  isRtl: PropTypes.bool,
  isNarrow: PropTypes.bool,
};

export default Side;
