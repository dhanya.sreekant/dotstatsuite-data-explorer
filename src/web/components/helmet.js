import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getIsRtl, getLocale } from '../selectors/router';

const View = ({ isRtl, lang }) => <Helmet htmlAttributes={{ lang, dir: isRtl ? 'rtl' : 'ltr' }} />;

View.propTypes = {
  isRtl: PropTypes.bool,
  lang: PropTypes.string,
};

export default connect(
  createStructuredSelector({
    isRtl: getIsRtl,
    lang: getLocale,
  }),
)(View);
