import React from 'react';
import { FormattedMessage /*FormattedHTMLMessage*/ } from 'react-intl';
// import { Tooltip } from '@sis-cc/dotstatsuite-visions';
// import HelpIcon from '@material-ui/icons/Help';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const FILTERS_ID = 'filters';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
  },
}));

const FiltersHelp = () => {
  const classes = useStyles();
  return (
    <Typography variant="body1" className={classes.root} id={FILTERS_ID} tabIndex={-1}>
      <FormattedMessage id="de.side.filters.action" />
      {/* pending feature */}
      {/* &nbsp;
      <Tooltip
        placement="bottom-start"
        title={
          <Typography variant="body2">
            <FormattedHTMLMessage id="de.filters.help" />
          </Typography>
        }
      >
        <HelpIcon fontSize="small" />
      </Tooltip> */}
    </Typography>
  );
};

FiltersHelp.propTypes = {};

export default FiltersHelp;
