import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import TuneIcon from '@material-ui/icons/Tune';
import ShareIcon from '@material-ui/icons/Share';
import TableChartOutlinedIcon from '@material-ui/icons/TableChartOutlined';
import FullscreenIcon from '@material-ui/icons/Fullscreen';
import FullscreenExitIcon from '@material-ui/icons/FullscreenExit';
import { Button } from './helpers';
import { FormattedMessage } from 'react-intl';
import Labels from './Labels';
import Charts from './Charts';
import Downloads from './Downloads';
import More from './More';

const FULLSCREEN = 'fullscreen';
const TABLE = 'table';
const SHARE = 'share';
const CONFIG = 'config';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    justifyContent: 'space-between',
  },
}));

const Component = props => {
  const classes = useStyles();
  // icon dry
  // usemenu dry
  // xs display
  useEffect(() => {
    if (R.and(R.isNil(props.isOpening), props.isFull)) {
      document.getElementById(FULLSCREEN).focus();
      props.changeFullscreen(true, true)
    }
  }, [props.isFull]);

  return (
    <Toolbar data-testid="detoolbar" className={classes.root} disableGutters>
      <div>
        <Button
          startIcon={<TableChartOutlinedIcon />}
          onClick={() => props.changeViewer(TABLE)}
          selected={R.equals(props.viewerId, TABLE)}
        >
          <FormattedMessage id="de.visualisation.toolbar.table" />
        </Button>
        <Charts
          chart={props.viewerId}
          changeChart={props.changeViewer}
          selected={R.not(R.equals(props.viewerId, TABLE))}
          hasRefAreaDimension={props.hasRefAreaDimension}
        />
      </div>
      <div>
        <Labels label={props.dimensionGetter} changeLabel={props.changeDimensionGetter} />
        <Button
          startIcon={<TuneIcon />}
          onClick={() => props.changeActionId(CONFIG)}
          aria-expanded={R.equals(props.actionId, CONFIG)}
          selected={R.equals(props.actionId, CONFIG)}
        >
          <FormattedMessage id="de.visualisation.toolbar.action.customize" />
        </Button>
        <Button
          startIcon={<ShareIcon />}
          onClick={() => props.changeActionId(SHARE)}
          selected={R.equals(props.actionId, SHARE)}
          aria-expanded={R.equals(props.actionId, SHARE)}
          loading={props.isSharing}
        >
          <FormattedMessage id="de.visualisation.toolbar.action.share" />
        </Button>
        <Downloads
          loading={R.or(props.isDownloading, props.isComputingExcel)}
          download={props.download}
          externalResources={props.externalResources}
        />
        <Button
          id={FULLSCREEN}
          startIcon={props.isFull ? <FullscreenExitIcon /> : <FullscreenIcon />}
          onClick={() => props.changeFullscreen(R.not(props.isFull))}
          selected={props.isFull}
          aria-pressed={props.isFull}
        >
          <FormattedMessage id="de.visualisation.toolbar.action.fullscreen" />
        </Button>
        <More actionId={props.actionId} changeActionId={props.changeActionId} />
      </div>
    </Toolbar>
  );
};

Component.propTypes = {
  changeActionId: PropTypes.func.isRequired,
  viewerId: PropTypes.string,
  actionId: PropTypes.string,
  changeViewer: PropTypes.func.isRequired,
  changeDimensionGetter: PropTypes.func.isRequired,
  changeFullscreen: PropTypes.func.isRequired,
  dimensionGetter: PropTypes.string,
  isFull: PropTypes.bool,
  isOpening: PropTypes.bool,
  isDownloading: PropTypes.bool,
  isComputingExcel: PropTypes.bool,
  download: PropTypes.func.isRequired,
  isSharing: PropTypes.bool,
  hasRefAreaDimension: PropTypes.bool,
  externalResources: PropTypes.array,
};

export default Component;
