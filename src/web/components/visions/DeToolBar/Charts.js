import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { makeStyles } from '@material-ui/core/styles';
import AssessmentOutlinedIcon from '@material-ui/icons/AssessmentOutlined';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Globe from '@material-ui/icons/Public';
import {
  Bar,
  Row,
  Scatter,
  HSymbol,
  VSymbol,
  Timeline,
  StackedBar,
} from '@sis-cc/dotstatsuite-visions';
import { FormattedMessage } from 'react-intl';
import { Button, Menu } from './helpers';
import { mapOptions } from '../../../lib/settings';

const charts = [
  { Icon: Bar, key: 'bar', id: 'BarChart' },
  { Icon: Row, key: 'row', id: 'RowChart' },
  { Icon: Scatter, key: 'scatter', id: 'ScatterChart' },
  { Icon: HSymbol, key: 'horizontalsymbol', id: 'HorizontalSymbolChart' },
  { Icon: VSymbol, key: 'verticalsymbol', id: 'VerticalSymbolChart' },
  { Icon: Timeline, key: 'timeline', id: 'TimelineChart' },
  { Icon: StackedBar, key: 'stacked', id: 'StackedBarChart' },
];

const useStyles = makeStyles(theme => ({
  icon: {
    minWidth: theme.spacing(4),
  },
}));

const Component = ({ chart, changeChart, selected, hasRefAreaDimension }) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const openMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const closeMenu = () => {
    setAnchorEl(null);
  };

  const itemClick = (onClickHandler, ...args) => () => {
    if (R.is(Function)(onClickHandler)) onClickHandler(...args);
    closeMenu();
  };

  return (
    <React.Fragment>
      <Button
        startIcon={<AssessmentOutlinedIcon />}
        selected={R.or(selected, Boolean(anchorEl))}
        onClick={openMenu}
        aria-haspopup="true"
      >
        <FormattedMessage id="de.visualisation.toolbar.chart" />
      </Button>
      <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={closeMenu}>
        {R.map(({ id, key, Icon }) => (
          <MenuItem
            key={id}
            onClick={itemClick(changeChart, id)}
            selected={R.equals(chart, id)}
            aria-pressed={R.equals(chart, id)}
            dense
          >
            <ListItemIcon className={classes.icon}>
              <Icon fontSize="small" color="primary" />
            </ListItemIcon>
            <ListItemText primaryTypographyProps={{ color: 'primary' }}>
              <FormattedMessage id={`de.visualisation.toolbar.chart.${key}`} />
            </ListItemText>
          </MenuItem>
        ))(charts)}
        {hasRefAreaDimension &&
          R.map(
            ({ id, mapId, levelId }) => (
              <MenuItem
                key={id}
                onClick={itemClick(changeChart, id, { map: { mapId, levelId } })}
                selected={R.equals(chart, id)}
                dense
              >
                <ListItemIcon className={classes.icon}>
                  <Globe fontSize="small" color="primary" />
                </ListItemIcon>
                <ListItemText primaryTypographyProps={{ color: 'primary' }}>
                  <FormattedMessage
                    id="chart.choropleth"
                    values={{ map: <FormattedMessage id={`map.${mapId}.${levelId}`} /> }}
                  />
                </ListItemText>
              </MenuItem>
            ),
            mapOptions,
          )}
      </Menu>
    </React.Fragment>
  );
};

Component.propTypes = {
  changeChart: PropTypes.func.isRequired,
  chart: PropTypes.string,
  selected: PropTypes.bool,
  hasRefAreaDimension: PropTypes.bool,
};

export default Component;
