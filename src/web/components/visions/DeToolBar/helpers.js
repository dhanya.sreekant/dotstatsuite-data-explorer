import React from 'react';
import PropTypes from 'prop-types';
import { VerticalButton } from '@sis-cc/dotstatsuite-visions';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import MuiMenu from '@material-ui/core/Menu';

export const Button = props => {
  const isXS = useMediaQuery((theme) => theme.breakpoints.down('xs'));

  return (
    <VerticalButton size="small" color="primary" {...props}>
      {isXS ? null : props.children}
    </VerticalButton>
  );
};

Button.propTypes = {
  children: PropTypes.node,
};

export const Menu = props => (
  <MuiMenu
    keepMounted
    getContentAnchorEl={null}
    anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
    transformOrigin={{ vertical: 'top', horizontal: 'center' }}
    {...props}
  >
    {props.children}
  </MuiMenu>
);

Menu.propTypes = {
  children: PropTypes.node,
};
