import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import LocalOfferOutlinedIcon from '@material-ui/icons/LocalOfferOutlined';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import { FormattedMessage } from 'react-intl';
import { Button, Menu } from './helpers';

const Component = ({ label, changeLabel }) => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const openMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const closeMenu = () => {
    setAnchorEl(null);
  };

  const itemClick = (onClickHandler, ...args) => () => {
    if (R.is(Function)(onClickHandler)) onClickHandler(...args);
    closeMenu();
  };

  return (
    <React.Fragment>
      <Button
        startIcon={<LocalOfferOutlinedIcon />}
        selected={Boolean(anchorEl)}
        onClick={openMenu}
        aria-haspopup="true"
      >
        <FormattedMessage id="de.visualisation.toolbar.action.labels" />
      </Button>
      <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={closeMenu}>
        {R.map(id => (
          <MenuItem
            key={id}
            onClick={itemClick(changeLabel, id)}
            selected={R.equals(label, id)}
            aria-pressed={R.equals(label, id)}
            dense
          >
            <ListItemText primaryTypographyProps={{ color: 'primary' }}>
              <FormattedMessage id={`vx.config.display.${id}`} />
            </ListItemText>
          </MenuItem>
        ))(['label', 'code', 'both'])}
      </Menu>
    </React.Fragment>
  );
};

Component.propTypes = {
  changeLabel: PropTypes.func.isRequired,
  label: PropTypes.string,
};

export default Component;
