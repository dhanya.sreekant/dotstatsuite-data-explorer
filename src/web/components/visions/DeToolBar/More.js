import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { makeStyles } from '@material-ui/core/styles';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import DeveloperModeOutlinedIcon from '@material-ui/icons/DeveloperModeOutlined';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import { FormattedMessage } from 'react-intl';
import { Button, Menu } from './helpers';

const API = 'api';

const useStyles = makeStyles(theme => ({
  icon: {
    minWidth: theme.spacing(4),
  },
}));

const Component = ({ actionId, changeActionId }) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const openMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const closeMenu = () => {
    setAnchorEl(null);
  };

  const itemClick = (onClickHandler, ...args) => () => {
    if (R.is(Function)(onClickHandler)) onClickHandler(...args);
    closeMenu();
  };

  return (
    <React.Fragment>
      <Button
        startIcon={<MoreVertIcon />}
        selected={R.or(R.contains(actionId, [API]), Boolean(anchorEl))}
        onClick={openMenu}
        aria-haspopup="true"
        aria-pressed={R.or(R.contains(actionId, [API]), Boolean(anchorEl))}
      >
        <FormattedMessage id="de.visualisation.toolbar.action.more" />
      </Button>
      <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={closeMenu}>
        <MenuItem
          onClick={itemClick(changeActionId, API)}
          selected={R.equals(actionId, API)}
          aria-expanded={R.equals(actionId, API)}
          dense
        >
          <ListItemIcon className={classes.icon}>
            <DeveloperModeOutlinedIcon fontSize="small" color="primary" />
          </ListItemIcon>
          <ListItemText primaryTypographyProps={{ color: 'primary' }}>
            <FormattedMessage id="de.visualisation.toolbar.action.apiqueries" />
          </ListItemText>
        </MenuItem>
      </Menu>
    </React.Fragment>
  );
};

Component.propTypes = {
  actionId: PropTypes.string,
  changeActionId: PropTypes.func.isRequired,
};

export default Component;
