import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Divider from '@material-ui/core/Divider';
import { FormattedMessage } from 'react-intl';
import User from '../../user';
import AccessibilityButton from '../../AccessibilityButton';
import useStyles from './useStyles';

const Component = ({ logo, locales, localeId, changeLocale }) => {
  const classes = useStyles();

  return (
    <AppBar data-testid="sisccappbar" position="static" className={classes.appBar} elevation={0}>
      <Toolbar className={classes.toolBar}>
        <div className={classes.logoWrapper}>
          <img className={classes.logo} src={logo} alt="siscc logo" />
        </div>
        <AccessibilityButton />
        <Divider orientation="vertical" className={classes.divider} />
        <User />
        <Divider orientation="vertical" className={classes.divider} />
        <TextField
          select
          value={localeId}
          variant="outlined"
          onChange={event => changeLocale(event.target.value)}
          SelectProps={{
            classes: { root: classes.select },
            MenuProps: {
              getContentAnchorEl: null,
              anchorOrigin: { vertical: 'bottom', horizontal: 'center' },
              transformOrigin: { vertical: 'top', horizontal: 'center' }, 
              classes: { paper: classes.paper } },
          }}
        >
          {R.map(id => (
            <MenuItem key={id} value={id} dense className={classes.menuItem}>
              <FormattedMessage id={id} />
            </MenuItem>
          ))(locales)}
        </TextField>
      </Toolbar>
    </AppBar>
  );
};

Component.propTypes = {
  logo: PropTypes.string,
  locales: PropTypes.array.isRequired,
  localeId: PropTypes.string,
  changeLocale: PropTypes.func.isRequired,
};

export default Component;
