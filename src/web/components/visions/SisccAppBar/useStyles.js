import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(theme => ({
  appBar: {
    background: theme.palette.grey[100],
    minWidth: 300,
  },
  toolBar: {
    paddingLeft: '5%',
    paddingRight: '5%',
  },
  logoWrapper: {
    flexGrow: 1,
  },
  logo: {
    maxHeight: 45,
    width: 'auto',
  },
  select: {
    padding: theme.spacing(1, 4, 1, 2),
    ...theme.typography.body2,
  },
  menuItem: {
    color: theme.palette.primary.main,
  },
  paper: {
    maxHeight: 320,
  },
  divider: {
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
    backgroundColor: 'transparent',
  },
}));
