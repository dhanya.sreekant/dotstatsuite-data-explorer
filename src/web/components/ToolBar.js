import * as R from 'ramda';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, withProps } from 'recompose';
import ToolBar from './visions/DeToolBar';
import { changeActionId, changeDimensionGetter, downloadExcel, changeFullscreen } from '../ducks/vis';
import { changeViewer } from '../ducks/router';
import { requestDataFile } from '../ducks/sdmx';
import { getViewer, getDataflow } from '../selectors/router';
import { getIsPending } from '../selectors/app';
import { getRefAreaDimension, getExternalResources } from '../selectors/sdmx';
import {
  getIsFull,
  getVisDimensionGetter,
  getIsComputingExcel,
  getIsSharing,
  getVisActionId,
  getIsOpeningFullscreen,
} from '../selectors';

export const DOWNLOAD = 'download';

export default compose(
  connect(
    createStructuredSelector({
      isDownloading: getIsPending('requestingDataFile'),
      isComputingExcel: getIsComputingExcel,
      dataflow: getDataflow,
      refAreaDimension: getRefAreaDimension,
      externalResources: getExternalResources,
      dimensionGetter: getVisDimensionGetter(),
      viewerId: getViewer,
      actionId: getVisActionId(),
      isSharing: getIsSharing(),
      isFull: getIsFull(),
      isOpening: getIsOpeningFullscreen
    }),
    {
      changeViewer,
      changeActionId,
      requestDataFile,
      downloadExcel,
      changeDimensionGetter,
      changeFullscreen,
    },
  ),
  withProps(({ requestDataFile, downloadExcel, dataflow, excelData, refAreaDimension }) => ({
    download: type => {
      if (R.equals(type, 'excel.selection')) return downloadExcel(excelData);
      if (R.equals(type, 'csv.selection')) return requestDataFile({ dataflow });
      if (R.equals(type, 'csv.all')) return requestDataFile({ isDownloadAllData: true, dataflow });
    },
    hasRefAreaDimension: R.not(R.isNil(refAreaDimension)),
  })),
)(ToolBar);
