import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '../AppBar';
import Footer from '../footer';
import Loader from '../loader';
import Page from '../Page';
import { ID_AUTH_PAGE } from '../../css-api';

const useStyles = makeStyles({
  root: {
    minHeight: '100vh',
  },
  content: {
    flexGrow: 1,
  },
});

const Component = ({ isFull, isAuthenticating, children }) => {
  const classes = useStyles({ isAuthenticating });

  return (
    <Grid container wrap="nowrap" direction="column" className={classes.root}>
      {R.not(isFull) && (
        <Grid item>
          <AppBar />
        </Grid>
      )}
      {isAuthenticating && (
        <Grid item className={classes.content}>
          <Page id={ID_AUTH_PAGE}>
            <Loader style={{ marginTop: 200 - 64 }} />
          </Page>
        </Grid>
      )}
      {R.not(isAuthenticating) && children}
      {R.not(isFull) && (
        <Grid item>
          <Footer />
        </Grid>
      )}
    </Grid>
  );
};

Component.propTypes = {
  isFull: PropTypes.bool,
  isAuthenticating: PropTypes.bool,
  children: PropTypes.array,
};

export default Component;
