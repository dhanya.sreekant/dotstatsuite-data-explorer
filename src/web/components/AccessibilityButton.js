import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';
import Button from '@material-ui/core/Button';
import AccessibilityNewIcon from '@material-ui/icons/AccessibilityNew';
import { getHasAccessibility } from '../selectors/router';
import { changeHasAccessibility } from '../ducks/router';

const AccessibilityButton = ({ hasAccessibility, changeHasAccessibility }) => (
  <Button onClick={() => changeHasAccessibility(!hasAccessibility)}>
    <AccessibilityNewIcon />
    {hasAccessibility ? <FormattedMessage id="accessibility.disable" /> : <FormattedMessage id="accessibility.enable" />}
  </Button>
);

AccessibilityButton.propTypes = {
  hasAccessibility: PropTypes.bool,
  changeHasAccessibility: PropTypes.func
};

export default connect(
  createStructuredSelector({ hasAccessibility: getHasAccessibility }),
  { changeHasAccessibility }
)(AccessibilityButton);

