import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, withProps } from 'recompose';
import { injectIntl } from 'react-intl';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { withStyles } from '@material-ui/core/styles';
import { ExpansionPanel } from '@sis-cc/dotstatsuite-visions';
import { changeFilter, applyDataAvailability } from '../../ducks/router';
import { getFilter, getDataAvailability, getParams } from '../../selectors/router';
import { PANEL_CONTENT_CONSTRAINTS } from '../../utils/constants';

const styles = {
  root: {
    marginLeft: 0,
  },
};

export const ContentConstraints = props => (
  <FormControl component="fieldset">
    <FormControlLabel
      classes={{ root: props.classes.root }}
      checked={!props.isChecked}
      control={
        <Checkbox
          onChange={() => props.onChange(!props.isChecked)}
          value="withoutContentConstraint"
        />
      }
      label={props.label}
    />
  </FormControl>
);

ContentConstraints.propTypes = {
  label: PropTypes.string,
  onChange: PropTypes.func,
  isChecked: PropTypes.bool,
  classes: PropTypes.object,
};

export const ActiveContentConstraints = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      dataAvailability: getDataAvailability,
    }),
    { onChange: applyDataAvailability },
  ),
  withProps(({ intl, dataAvailability }) => ({
    isChecked: dataAvailability,
    label: intl.formatMessage({ id: 'de.contentConstraints.checkbox.label' }),
  })),
  withStyles(styles),
)(ContentConstraints);

export const PanelContentConstraints = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      activePanelId: getFilter,
      params: getParams,
    }),
    { onChangeActivePanel: changeFilter },
  ),
  withProps(({ intl, activePanelId, params }) => ({
    isOpen: R.equals(PANEL_CONTENT_CONSTRAINTS, activePanelId),
    id: PANEL_CONTENT_CONSTRAINTS,
    label: intl.formatMessage({ id: 'de.panel.contentConstraints.title' }),
    isBlank: R.not(R.has('dataAvailability')(params))
  })),
)(ExpansionPanel);
