import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { CLASS_PAGE } from '../../css-api';

const styles = {
  root: {
    flexGrow: 1,
  },
};

const Component = ({ id, classes, children }) => (
  <div id={id} className={cx(classes.root, CLASS_PAGE)}>
    {children}
  </div>
);

Component.propTypes = {
  id: PropTypes.string.isRequired,
  children: PropTypes.node,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Component);
