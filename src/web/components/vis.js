import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, withProps } from 'recompose';
import cx from 'classnames';
import { FormattedMessage } from 'react-intl';
import { NoData } from '@sis-cc/dotstatsuite-visions';
import FiltersHelp from './filters-help';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { getIsFull } from '../selectors';
import { resetDataflow, resetDataflowAndSearch } from '../ducks/router';
import { getIsPending } from '../selectors/app';
import Loader from './loader';
import Side from './vis-side';
import * as Settings from '../lib/settings';
import { ParsedDataVisualisation } from './vis/vis-data';
import NarrowFilters from './vis-side/side-container';
import DeAppBar from './visions/DeAppBar';
import Page from './Page';
import { ID_VIS_PAGE } from '../css-api';

const useStyles = makeStyles(theme => ({
  subHeader: {
    display: 'flex',
    width: '100%',
    backgroundColor: theme.palette.primary.main,
    minHeight: 60,
  },
  margin: {
    padding: ({ isXS }) => `${theme.spacing(isXS ? 2 : 0.5)}px  5%`,
    margin: 0,
  },
  side: {
    minWidth: 300,
  },
  sidePadding: {
    paddingRight: theme.spacing(2),
  },
  main: {
    width: '100%',
  },
  backLink: {
    marginBottom: theme.spacing(2),
    '&:hover': {
      color: theme.palette.primary.main,
    },
  },
  backIcon: {
    fontSize: '0.75rem',
  },
}));

const Vis = ({ isLoadingStructure, resetDataflow, resetDataflowAndSearch, logo, isFull }) => {
  const theme = useTheme();
  const isXS = useMediaQuery(theme.breakpoints.down('xs'));
  const classes = useStyles({ isXS });
  const dataVisualisation = R.not(isLoadingStructure) ? <ParsedDataVisualisation /> : null;

  return (
    <Fragment>
      <Typography tabIndex={0} aria-live="assertive" variant="srOnly">
        {isLoadingStructure && <FormattedMessage id="de.visualisation.loading" />}
      </Typography>
      {R.not(isFull) && (
        <DeAppBar
          goHome={resetDataflowAndSearch}
          goBack={resetDataflow}
          goBackLabel={<FormattedMessage id="de.search.back" />}
          logo={logo}
        />
      )}
      <Page id={ID_VIS_PAGE}>
        {isLoadingStructure
          ? <Grid item>
              <Grid container className={classes.margin}>
                <NoData
                  icon={<Loader style={{ marginTop: 24 }} />}
                  message={<FormattedMessage id="de.visualisation.loading" />}
                />
              </Grid>
            </Grid>
          : <Grid item>
              {R.not(isFull) && (
                <Grid container className={classes.margin} style={{ paddingBottom: 0 }}>
                  <FiltersHelp />
                </Grid>
               )}
              <Grid container className={isFull ? null : classes.margin} wrap={isXS ? 'wrap' : 'nowrap'}>
                {R.not(isFull) && (
                  <Grid
                    item xs={12} sm={2}
                    className={cx(classes.side, { [classes.sidePadding]: R.not(isXS) })}
                  >
                    <NarrowFilters isXS={isXS}>
                      <Side />
                    </NarrowFilters>
                  </Grid>
                )}
                <Grid item xs={12} sm={isFull ? 12 :10} className={classes.main}>
                  {dataVisualisation}
                </Grid>
              </Grid>
            </Grid>
        }
      </Page>
    </Fragment>
  );
};

Vis.propTypes = {
  isNarrow: PropTypes.bool,
  isFull: PropTypes.bool,
  isLoadingStructure: PropTypes.bool,
  logo: PropTypes.string,
  resetDataflow: PropTypes.func.isRequired,
  resetDataflowAndSearch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  isLoadingStructure: getIsPending('getStructure'),
  isFull: getIsFull(),
});

export default compose(
  connect(mapStateToProps, { resetDataflow, resetDataflowAndSearch }),
  withProps(() => ({
    logo: Settings.getAsset('subheader'),
  })),
)(Vis);
