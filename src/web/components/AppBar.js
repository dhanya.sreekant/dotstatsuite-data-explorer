import * as R from 'ramda';
import { withProps } from 'recompose';
import { withLocale } from '../i18n';
import { getAsset, locales } from '../lib/settings';
import AppBar from './visions/SisccAppBar';

export default R.compose(
  withProps({
    logo: getAsset('header'),
    locales: R.keys(locales),
  }),
  withLocale,
)(AppBar);
