import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, mapProps, withProps } from 'recompose';
import * as R from 'ramda';
import { RulesDriver, Viewer as ViewerComp } from '@sis-cc/dotstatsuite-components';
import { Table, TableHtml5, Cell, ValueCell } from '@sis-cc/dotstatsuite-visions';
import { useTheme, makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import Tools from '../vis-tools';
import { share } from '../../ducks/vis';
import {
  getShareMode,
  getVisDataDimensions,
  getVisTableLayout,
  getVisDimensionGetter,
  getTableProps,
  getHeaderProps,
  getVisChoroMap,
  getVisIsLoadingMap,
  getIsTimeInverted,
} from '../../selectors';
import { getIsPending } from '../../selectors/app';
import { getViewer, getDataflow, getHasAccessibility } from '../../selectors/router';
import {
  getData,
  getDataUrl,
  getTimelineAxisProc,
  getDataRange,
  getDataSourceHeaders,
  getSelection,
} from '../../selectors/sdmx';
import * as Settings from '../../lib/settings';
import { withLocale } from '../../i18n';
import withRefinedViewerProps from '../../utils/viewer';
import { options as viewerOptions } from '../../lib/viewer';
import { getIsRtl } from '../../theme/utils';

const getHeaderDisclaimer = (range = {}, type) => {
  const { count, total } = R.pick(['count', 'total'], range);
  if (R.any(R.isNil, [count, total])) {
    return null;
  }
  if (count < total) {
    const id = type === 'table' ? 'incomplete.table.data' : 'incomplete.chart.data';
    return <FormattedMessage id={id} values={{ range: count }} />;
  }
  return null;
};

const useStyles = makeStyles(theme => ({
  divider: {
    borderTop: 'solid 2px #999',
    paddingTop: theme.spacing(0.5),
  },
}));

const DataVisView = ({
  isLoadingData,
  isLoadingMap,
  isNarrow,
  isFull,
  toolsProps,
  viewerProps,
  hasSelection,
  hasAccessibility
}) => {
  const theme = useTheme();
  const classes = useStyles();
  const isRtl = getIsRtl(theme);
  const loaderId = R.cond([
    [R.always(isLoadingData), R.always('de.visualisation.data.loading')],
    [R.always(isLoadingMap), R.always('de.vis.map.loading')],
    [R.T, R.identity],
  ])(null);
  const loading = loaderId ? <FormattedMessage id={loaderId} /> : null;
  const noDataId = R.cond([
    [R.always(hasSelection), R.always('vx.no.data.selection')],
    [R.T, R.always('vx.no.data.available')],
  ])(null);

  return (
    <div className={classes.divider}>
      <Tools
        isNarrow={isNarrow}
        isRtl={isRtl}
        isFull={isFull}
        {...toolsProps}
        excelData={{ ...viewerProps, theme }}
      />
      <ViewerComp
        {...viewerProps}
        TableComponent={hasAccessibility ? TableHtml5 : Table }
        CellComponent={hasAccessibility ? Cell : ValueCell }
        loading={loading}
        isNarrow={isNarrow}
        isRtl={isRtl}
        noData={<FormattedMessage id={noDataId} />}
      />
      <Typography tabIndex={0} aria-live="assertive" variant="srOnly">
        {loading && <FormattedMessage id="de.visualisation.data.loading" />}
      </Typography>
    </div>
  );
};

const DataVis = compose(
  connect(
    createStructuredSelector({
      dataflow: getDataflow,
      dimensions: getVisDataDimensions(),
      isLoadingData: getIsPending('getData'),
      isLoadingMap: getVisIsLoadingMap,
      isTimeInverted: getIsTimeInverted(),
      layoutIds: getVisTableLayout(),
      shareMode: getShareMode(),
      tableProps: getTableProps(),
      headerProps: getHeaderProps(),
      range: getDataRange,
      sourceHeaders: getDataSourceHeaders,
      selection: getSelection,
      hasAccessibility: getHasAccessibility
    }),
    { share },
  ),
  withProps(({ headerProps, range, type }) => ({
    fonts: viewerOptions.fonts,
    footerProps: {
      ...R.omit(['fonts'], viewerOptions),
      source: {
        label: R.path(['title', 'label'], headerProps),
        link: window.location.href,
      },
    },
    headerProps: R.assoc('disclaimer', getHeaderDisclaimer(range, type))(headerProps),
  })),
  withProps(({ data, dataflow, locale, sdmxUrl, tableProps, selection }) => ({
    cellsLimit: Settings.cellsLimit,
    customAttributes: Settings.customAttributes,
    dataflowId: dataflow.dataflowId,
    isNotATable: R.hasPath(['cells', '', '', '', 0], tableProps),
    source: sdmxUrl,
    isNonIdealState: R.isNil(data),
    locale: R.pathOr({ id: locale }, ['i18n', 'locales', locale], Settings),
    hasSelection: R.not(R.isEmpty(selection)),
  })),
  withRefinedViewerProps,
)(DataVisView);

export const ParsedDataVisualisation = compose(
  connect(
    createStructuredSelector({
      display: getVisDimensionGetter(),
      type: getViewer,
      data: getData,
      timelineAxisProc: getTimelineAxisProc,
      dataUrl: getDataUrl({ agnostic: false }),
      map: getVisChoroMap,
    }),
  ),
  withLocale,
  mapProps(({ dataUrl, localeId, timelineAxisProc, map, type, ...rest }) => {
    const isNonIdealMapState = R.allPass([
      R.always(type === 'ChoroplethChart'),
      R.anyPass([R.isNil, R.has('error')]),
    ])(map);
    return {
      sdmxUrl: dataUrl,
      formaterIds: {
        decimals: Settings.getSdmxAttribute('decimals'),
        prefscale: Settings.getSdmxAttribute('prefscale'),
      },
      locale: localeId,
      options: R.when(
        R.always(type === 'TimelineChart'),
        R.set(R.lensPath(['axis', 'x', 'format', 'proc']), timelineAxisProc),
      )(Settings.chartOptions),
      source: { link: window.location.href },
      map: R.when(R.always(isNonIdealMapState), R.always(null))(map),
      type,
      isNonIdealMapState,
      ...rest,
    };
  }),
)(props => (
  <RulesDriver
    {...props}
    render={(
      parsedProps, // chartData, chartOptions, properties (aka chart configs props)
    ) => (
      <DataVis
        {...parsedProps}
        {...R.pick(
          ['data', 'display', 'formaterIds', 'locale', 'type', 'isNonIdealMapState', 'sdmxUrl'],
          props,
        )}
      />
    )}
  />
));
