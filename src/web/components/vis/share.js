import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, withProps } from 'recompose';
import * as R from 'ramda';
import { Share } from '@sis-cc/dotstatsuite-visions';
import { changeShareMail, changeShareMode, closeSharePopup } from '../../ducks/vis';
import {
  getHasShared,
  getIsSharing,
  getShareError,
  getShareMail,
  getShareMode,
} from '../../selectors';
import { FormattedMessage } from 'react-intl';

const privacyPolicy = R.pathOr('', ['SETTINGS', 'share', 'policy'], window);

const mapStateToProps = createStructuredSelector({
  error: getShareError(),
  hasShared: getHasShared(),
  isSharing: getIsSharing(),
  mail: getShareMail(),
  mode: getShareMode(),
});

const mapDispatchToProps = {
  changeMail: changeShareMail,
  changeMode: changeShareMode,
  changeHasShared: closeSharePopup,
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withProps(({ error }) => ({
    modes: [
      {
        label: <FormattedMessage id="share.snapshot" />,
        value: 'snapshot',
      },
      {
        label: <FormattedMessage id="share.latest" />,
        value: 'latest',
      },
    ],
    labels: {
      disclaimer: (
        <FormattedMessage
          id="share.disclaimer"
          values={{
            br: <br />,
            link: (
              <a target="_blank" rel="noopener noreferrer" href={privacyPolicy || '#'}>
                <FormattedMessage id="share.policy.link" />
              </a>
            ),
          }}
        />
      ),
      email: <FormattedMessage id="share.mail" />,
      errorTitle: <FormattedMessage id="share.error.title" />,
      errorMessage: <FormattedMessage id="share.error.message" />,
      title: <FormattedMessage id="share.title" />,
      submit: <FormattedMessage id="share.action" />,
      successTitle: <FormattedMessage id="share.success.title" />,
      successMessage: <FormattedMessage id="share.success.message" />,
    },
    hasError: R.or(R.not(R.isNil(error)), R.not(R.isEmpty(error))),
  })),
)(Share);
