import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, onlyUpdateForKeys, withProps } from 'recompose';
import * as R from 'ramda';
import ToolBar from './ToolBar';
import { TableLayout, ChartsConfig } from '@sis-cc/dotstatsuite-visions';
import Paper from '@material-ui/core/Paper';
import Collapse from '@material-ui/core/Collapse';
import { makeStyles } from '@material-ui/core';
import { FormattedMessage, injectIntl } from 'react-intl';
import { changeActionId, changeLayout, changeIsTimeDimensionInverted } from '../ducks/vis';
import {
  getVisActionId,
  getIsSharing,
  getVisDimensionLayout,
  getVisDimensionFormat,
  getTimeDimensionInverted,
} from '../selectors';
import { getTimePeriod } from '../selectors/sdmx';
import { getViewer } from '../selectors/router';
import APIQueries from './vis/api-queries';
import ShareView from './vis/share';

const useStyles = makeStyles(theme => ({
  container: {
    margin: theme.spacing(1, 0),
    padding: theme.spacing(2),
  },
}));

//-----------------------------------------------------------------------------------------Constants
const API = 'api';
const TABLE = 'table';
const FILTERS = 'filters';
const CONFIG = 'config';
const SHARE = 'share';

//-----------------------------------------------------------------------------------------ToolTable
export const ToolTable = compose(
  connect(
    createStructuredSelector({
      layout: getVisDimensionLayout,
      itemRenderer: getVisDimensionFormat(),
      itemButton: getTimeDimensionInverted(),
      timePeriod: getTimePeriod,
    }),
    { changeLayout, changeIsTimeDimensionInverted },
  ),
  injectIntl,
  onlyUpdateForKeys(['layout', 'timePeriod', 'itemRenderer', 'itemButton']),
)(
  ({
    layout,
    changeLayout,
    itemButton,
    itemRenderer,
    timePeriod,
    intl,
    changeIsTimeDimensionInverted,
  }) => {
    const asc = intl.formatMessage({ id: 'de.table.layout.time.asc' });
    const desc = intl.formatMessage({ id: 'de.table.layout.time.desc' });
    const timePeriodId = R.propOr('', 'id')(timePeriod);
    const isTimeDimensionInverted = R.propOr(false, timePeriodId)(itemButton);
    const timePeriodButton = R.pipe(
      R.set(R.lensProp('value'), isTimeDimensionInverted ? desc : asc ),
      R.set(R.lensProp('options'), [asc, desc]),
      R.set(R.lensProp('onChange'), v =>
        changeIsTimeDimensionInverted(timePeriodId, R.equals(desc)(v)),
      ),
    )(timePeriod);
    return (
      <TableLayout
        layout={layout}
        commit={changeLayout}
        itemButton={itemButton}
        itemRenderer={itemRenderer}
        itemButtonProps={{
          [timePeriodId]: timePeriodButton,
        }}
        labels={{
          commit: <FormattedMessage id="de.table.layout.apply" />,
          cancel: <FormattedMessage id="de.table.layout.cancel" />,
          row: <FormattedMessage id="de.table.layout.x" />,
          column: <FormattedMessage id="de.table.layout.y" />,
          section: <FormattedMessage id="de.table.layout.z" />,
          d: <FormattedMessage id="de.table.layout.getter.dimension" />,
          o: <FormattedMessage id="de.table.layout.getter.observation" />,
          time: <FormattedMessage id="de.table.layout.time" />,
          asc: <FormattedMessage id="de.table.layout.time.asc" />,
          desc: <FormattedMessage id="de.table.layout.time.desc" />,
          help: <FormattedMessage id="de.table.layout.help" />,
          table: <FormattedMessage id="de.table.layout.table" />,
          one: <FormattedMessage id="de.table.layout.value.one" />,
        }}
      />
    );
  },
);

//---------------------------------------------------------------------------------------ChartConfig
const renameKey = R.curry((oldKey, newKey, obj) =>
  R.assoc(newKey, R.prop(oldKey, obj), R.dissoc(oldKey, obj)),
);

const renameProperties = (keys = []) =>
  R.map(
    R.ifElse(
      R.pipe(R.prop('id'), R.flip(R.includes)(keys)),
      renameKey('onChange', 'onSubmit'),
      R.identity,
    ),
  );

const propertiesKeys = [
  'width',
  'height',
  'freqStep',
  'maxX',
  'minX',
  'pivotX',
  'stepX',
  'maxY',
  'minY',
  'pivotY',
  'stepY',
];

export const Config = compose(
  injectIntl,
  withProps(({ intl, properties }) => {
    const labels = {
      focus: intl.formatMessage({ id: 'chart.config.focus' }),
      highlight: intl.formatMessage({ id: 'chart.config.highlights' }),
      select: intl.formatMessage({ id: 'chart.config.select' }),
      baseline: intl.formatMessage({ id: 'chart.config.baseline' }),
      size: intl.formatMessage({ id: 'chart.config.size' }),
      width: intl.formatMessage({ id: 'chart.config.width' }),
      height: intl.formatMessage({ id: 'chart.config.height' }),
      display: intl.formatMessage({ id: 'de.table.layout.getter.dimension' }),
      displayOptions: {
        label: intl.formatMessage({ id: 'vx.config.display.label' }),
        code: intl.formatMessage({ id: 'vx.config.display.code' }),
        both: intl.formatMessage({ id: 'vx.config.display.both' }),
      },
      series: intl.formatMessage({ id: 'chart.config.series' }),
      scatterDimension: intl.formatMessage({
        id: 'chart.config.dimension.x.y.axes',
      }),
      scatterX: intl.formatMessage({
        id: 'chart.config.dimension.values.x.axis',
      }),
      scatterY: intl.formatMessage({
        id: 'chart.config.dimension.values.y.axis',
      }),
      symbolDimension: intl.formatMessage({
        id: 'chart.config.dimension.for.symbols',
      }),
      stackedDimension: intl.formatMessage({
        id: 'chart.config.dimension.x.axis',
      }),
      stackedMode: intl.formatMessage({
        id: 'chart.config.observations.displayed',
      }),
      stackedModeOptions: {
        values: intl.formatMessage({ id: 'chart.config.stacked.values' }),
        percent: intl.formatMessage({ id: 'chart.config.stacked.percent' }),
      },
      axisX: intl.formatMessage({ id: 'chart.config.x.axis' }),
      axisY: intl.formatMessage({ id: 'chart.config.y.axis' }),
      max: intl.formatMessage({ id: 'chart.config.maximum' }),
      min: intl.formatMessage({ id: 'chart.config.minimum' }),
      pivot: intl.formatMessage({ id: 'chart.config.pivot' }),
      frequency: intl.formatMessage({ id: 'chart.config.step.period' }),
      freqStep: intl.formatMessage({ id: 'chart.config.step' }),
    };
    return {
      labels,
      properties: R.pipe(
        R.omit(['title', 'subtitle', 'sourceLabel']),
        renameProperties(propertiesKeys),
      )(properties),
    };
  }),
)(ChartsConfig);

//---------------------------------------------------------------------------------------------Tools
export default compose(
  connect(
    createStructuredSelector({
      actionId: getVisActionId(),
      viewerId: getViewer,
      isSharing: getIsSharing(),
    }),
    { changeActionId },
  ),
  withProps(({ actionId, viewerId }) => ({
    isApi: R.equals(API)(actionId),
    isTableConfig: R.equals(CONFIG)(actionId) && R.equals(TABLE)(viewerId),
    isChartConfig: R.equals(CONFIG)(actionId) && !R.equals(TABLE)(viewerId),
    isShare: R.equals(SHARE)(actionId),
    isFilters: R.equals(FILTERS)(actionId),
  })),
)(
  ({
    isNarrow,
    isRtl,
    isApi,
    isTableConfig,
    isChartConfig,
    isShare,
    properties,
    share,
    excelData,
  }) => {
    const classes = useStyles();
    return (
      <Fragment>
        <ToolBar excelData={excelData} />
        <Collapse in={isApi}>
          <Paper elevation={2} className={classes.container}>
            <APIQueries isNarrow={isNarrow} isRtl={isRtl} />
          </Paper>
        </Collapse>
        <Collapse in={isTableConfig}>
          <Paper elevation={2} className={classes.container}>
            <ToolTable isNarrow={isNarrow} isRtl={isRtl} />
          </Paper>
        </Collapse>
        <Collapse in={isChartConfig}>
          <Paper elevation={2} className={classes.container}>
            <Config properties={properties} />
          </Paper>
        </Collapse>
        <Collapse in={isShare}>
          <Paper elevation={2}>
            <ShareView share={share} />
          </Paper>
        </Collapse>
      </Fragment>
    );
  },
);
