import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { withProps } from 'recompose';
import { DEFooter, DEFooterIcon, DEFooterLink } from '@sis-cc/dotstatsuite-ui-components';
import { getAsset } from '../lib/settings';

const Footer = ({ isNarrow, isRtl, logo }) => (
  <DEFooter
    isNarrow={isNarrow}
    isRtl={isRtl}
    authorLabel={
      <FormattedMessage
        id="de.footer.author"
        values={{
          icon: <DEFooterIcon src={logo} alt="icon" />,
          link: <DEFooterLink role='presentation' href="https://siscc.org/">SIS-CC</DEFooterLink>,
        }}
      />
    }
    disclaimerLabel={<FormattedMessage id="de.footer.disclaimer" values={{ br: <br /> }} />}
  />
);

Footer.propTypes = {
  isNarrow: PropTypes.bool,
  isRtl: PropTypes.bool,
  logo: PropTypes.string,
};

export default withProps({ logo: getAsset('footer') })(Footer);
