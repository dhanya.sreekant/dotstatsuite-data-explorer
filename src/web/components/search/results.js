import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { compose, withProps, pure } from 'recompose';
import cx from 'classnames';
import * as R from 'ramda';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import {
  Spotlight,
  Chips,
  ScopeList,
  ExpansionPanel,
  NoData,
  spotlightScopeListEngine,
  Tag,
} from '@sis-cc/dotstatsuite-visions';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { PANEL_SEARCH_CURRENT } from '../../utils/constants';
import NarrowFilters from '../vis-side/side-container';
import DeAppBar from '../visions/DeAppBar';
import FiltersHelp from '../filters-help';
import Loader from '../loader';
import Dataflows from './dataflows';
import Pagination from './pagination';
import { countNumberOf } from '../../utils';
import { getIsRtl } from '../../theme/utils';
import Page from '../Page';
import { ID_SEARCH_PAGE } from '../../css-api';

const Facet = pure(ScopeList);
const Facets = compose(
  withProps(({ intl }) => ({
    topElementProps: {
      fullWidth: true,
      hasClearAll: true,
      mainPlaceholder: intl.formatMessage({ id: 'vx.spotlight.placeholder.primary' }),
      secondaryPlaceholder: intl.formatMessage({ id: 'vx.spotlight.placeholder.secondary' }),
      defaultSpotlight: {
        engine: spotlightScopeListEngine,
        fields: {
          label: {
            id: 'label',
            accessor: R.pipe(R.propOr(null, 'label'), R.ifElse(R.isNil, R.always(''), R.identity)),
            isSelected: true,
          },
        },
      },
    },
    topElementComponent: values => (R.gte(R.length(values), 8) ? Spotlight : null),
  })),
)(({ topElementComponent, facets = [], intl, ...parentProps }) =>
  R.map(
    ({ id, label, values, /*hasPath,*/ isPinned }) => (
      <Facet
        {...parentProps}
        id={id}
        key={id}
        label={`${isPinned ? '*' : ''}${
          R.isNil(label) ? intl.formatMessage({ id: `de.search.${id}` }) : label
        }`}
        items={values}
        hasPath={false}
        TopElementComponent={topElementComponent(values)}
      />
    ),
    facets,
  ),
);

const useStyles = makeStyles(theme => ({
  margin: {
    padding: ({ isXS }) => `${theme.spacing(isXS ? 2 : 0.5)}px  5%`,
    margin: 0,
  },
  spotlightContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padddingTop: ({ isXS }) => theme.spacing(isXS ? 2 : 0),
  },
  side: {
    minWidth: 300,
  },
  sidePadding: {
    paddingRight: theme.spacing(2),
  },
  main: {
    width: '100%',
  },
}));

const Results = ({
  isLoading,
  isBlank,
  intl,
  facets,
  constraints,
  changeConstraints,
  term,
  changeTerm,
  changeFacet,
  facet,
  logo,
  resetSearch,
  size,
  accessibility,
}) => {
  const theme = useTheme();
  const isRtl = getIsRtl(theme);
  const isXS = useMediaQuery(theme.breakpoints.down('xs'));
  const classes = useStyles({ isXS });

  const NO_RESULT_MESSAGE_ID = 'noResultMessage';
  const SEARCH_RESULTS_COUNT_ID = 'searchResultsCount';

  useEffect(() => {
    if (R.not(accessibility)) return;
    if (R.not(isLoading)) {
      R.not(isBlank)
        ? document.getElementById(SEARCH_RESULTS_COUNT_ID).focus()
        : document.getElementById(NO_RESULT_MESSAGE_ID).focus();
    }
    if (R.and(R.not(isLoading), R.not(isBlank))) 
      document.getElementById(SEARCH_RESULTS_COUNT_ID).focus();
  }, [isLoading, isBlank]); //eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Fragment>
      <DeAppBar goHome={resetSearch} logo={logo}>
        <Spotlight
          hasClearAll
          hasCommit
          fullWidth
          action={changeTerm}
          placeholder={intl.formatMessage({ id: 'vx.spotlight.placeholder' })}
          isRtl={isRtl}
          term={term}
        />
      </DeAppBar>
      <Page id={ID_SEARCH_PAGE}>
        <Grid container>
          <Grid item xs={12} className={classes.main}>
            <Grid container className={classes.margin}>
              {R.or(R.and(isBlank, R.not(isLoading)), isLoading) && (
                <Grid item xs={12} style={{ marginTop: 200 - 64 * 2 }}>
                  {isLoading && (
                    <NoData
                      message={<FormattedMessage id="de.search.list.loading" />}
                      icon={<Loader />}
                    />
                  )}
                  {R.and(isBlank, R.not(isLoading)) && (
                    <NoData message={<FormattedMessage id="de.search.list.blank" />} />
                  )}
                </Grid>
              )}
              <Typography tabIndex={0} aria-live="assertive" variant="srOnly">
                {isLoading && <FormattedMessage id="de.search.list.loading" />}
              </Typography>
            </Grid>
          </Grid>
          {R.and(R.not(isLoading), R.not(isBlank)) && (
            <Grid item className={classes.main}>
              <Grid container className={classes.margin} wrap={isXS ? 'wrap' : 'nowrap'}>
                <Grid
                  item
                  xs={12}
                  sm={2}
                  className={cx(classes.side, { [classes.sidePadding]: R.not(isXS) })}
                >
                  <FiltersHelp />
                  <NarrowFilters isXS={isXS}>
                    {R.not(R.isEmpty(constraints)) && (
                      <ExpansionPanel
                        id={PANEL_SEARCH_CURRENT}
                        label={<FormattedMessage id="vx.filters.current.title" />}
                        onChangeActivePanel={changeFacet}
                        tag={<Tag>{countNumberOf(constraints)}</Tag>}
                      >
                        <Chips
                          items={constraints}
                          onDelete={changeConstraints}
                          onDeleteAll={changeConstraints}
                          clearAllLabel={<FormattedMessage id="vx.filters.current.clear" />}
                        />
                      </ExpansionPanel>
                    )}
                    <Facets
                      accessibility={accessibility}
                      intl={intl}
                      facets={facets}
                      activePanelId={facet}
                      onChangeActivePanel={changeFacet}
                      changeSelection={changeConstraints}
                    />
                  </NarrowFilters>
                </Grid>
                <Grid item xs={12} sm={10} className={classes.main}>
                  <Typography variant="body1" tabIndex={0} id={SEARCH_RESULTS_COUNT_ID}>
                    <FormattedMessage id="de.search.list.status" values={{ size }} />
                  </Typography>
                  <Dataflows />
                  <Grid container justify="flex-end">
                    <Pagination />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          )}
        </Grid>
      </Page>
    </Fragment>
  );
};

Results.propTypes = {
  isNarrow: PropTypes.bool,
  isRtl: PropTypes.bool,
  isLoading: PropTypes.bool,
  isBlank: PropTypes.bool,
  intl: PropTypes.object,
  constraints: PropTypes.array,
  facets: PropTypes.array,
  changeConstraints: PropTypes.func,
  term: PropTypes.string,
  changeTerm: PropTypes.func,
  changeFacet: PropTypes.func,
  facet: PropTypes.string,
  logo: PropTypes.string,
  resetSearch: PropTypes.func.isRequired,
  size: PropTypes.number,
  accessibility: PropTypes.bool,
};

export default Results;
