import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  compose,
  branch,
  renderComponent,
  withProps,
  withStateHandlers,
  lifecycle,
} from 'recompose';
import { injectIntl } from 'react-intl';
import * as R from 'ramda';
import Home from './home';
import Results from './results';
import { getIsPending } from '../../selectors/app';
import {
  getResultsFacets,
  getDataflows,
  getConstraints,
  getConfigFacets,
  getHasNoSearchParams,
  getNumFound,
} from '../../selectors/search';
import { changeTerm, changeConstraints, changeFacet } from '../../ducks/router';
import { requestConfig, resetSearch } from '../../ducks/search';
import { getTerm, getFacet, getHasAccessibility } from '../../selectors/router';
import { getAsset, hasNoSearch } from '../../lib/settings';

const setLabel = intl => ({ id, label, ...rest }) => ({
  ...rest,
  id,
  label: R.isNil(label) ? intl.formatMessage({ id: `de.search.${id}` }) : label,
});

const EnhancedHome = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      facets: getConfigFacets,
      isLoading: getIsPending('getConfig'),
    }),
    { changeTerm, changeConstraints, requestConfig },
  ),
  withProps(({ facets, intl }) => ({
    hasNoSearch,
    logo: getAsset('splash'),
    facets: R.ifElse(
      R.isEmpty,
      R.identity,
      R.map(
        R.pipe(
          setLabel(intl),
          R.over(
            R.lensProp('values'),
            R.ifElse(
              R.isEmpty,
              R.identity,
              R.pipe(
                R.filter(R.pipe(R.prop('level'), R.anyPass([R.isNil, isNaN, R.gte(1)]))),
                R.groupBy(
                  R.pipe(R.prop('parentId'), R.ifElse(R.isNil, R.always('orphans'), R.identity)),
                ),
                R.converge(
                  (orphans, children) =>
                    R.map(orphan =>
                      R.assoc(
                        'subtopics',
                        R.pipe(R.propOr([], R.prop('id', orphan)), R.pluck('label'))(children),
                        orphan,
                      ),
                    )(orphans),
                  [R.prop('orphans'), R.dissoc('orphans')],
                ),
              ),
            ),
          ),
        ),
      ),
    )(facets),
  })),
  lifecycle({
    componentDidMount() {
      this.props.requestConfig();
    },
  }),
)(Home);

const EnhancedResults = compose(
  connect(
    createStructuredSelector({
      dataflows: getDataflows,
      facets: getResultsFacets,
      constraints: getConstraints,
      isSearchLoading: getIsPending('getSearch'),
      isConfigLoading: getIsPending('getConfig'),
      term: getTerm,
      facet: getFacet,
      size: getNumFound,
      accessibility: getHasAccessibility
    }),
    {
      changeConstraints,
      changeFacet,
      changeTerm,
      resetSearch,
    },
  ),
  withStateHandlers(
    { actionId: undefined },
    {
      changeActionId: ({ actionId }) => nextActionId => ({
        actionId: R.ifElse(R.equals(actionId), R.always(null), R.identity)(nextActionId),
      }),
    },
  ),
  withProps(({ dataflows, isConfigLoading, isSearchLoading }) => ({
    logo: getAsset('subheader'),
    isLoading: R.or(isConfigLoading, isSearchLoading),
    isBlank: R.isEmpty(dataflows),
  })),
)(Results);

export default compose(
  injectIntl,
  connect(createStructuredSelector({ hasNoSearchParams: getHasNoSearchParams })),
  branch(({ hasNoSearchParams }) => hasNoSearchParams, renderComponent(EnhancedHome)),
)(EnhancedResults);
