import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, branch, renderNothing, withProps } from 'recompose';
import * as R from 'ramda';
import { getCurrentRows, getPage, getPages } from '../../selectors/search';
import { changeStart } from '../../ducks/router';
import { injectIntl } from 'react-intl';
import { Pagination } from '@sis-cc/dotstatsuite-visions';
import { FormattedMessage } from 'react-intl';

const mapStateToProps = createStructuredSelector({
  page: getPage,
  pages: getPages,
  rows: getCurrentRows,
});

const mapDispatchToProps = {
  changeStart,
};

export default compose(
  injectIntl,
  connect(mapStateToProps, mapDispatchToProps),
  branch(({ pages, rows }) => R.isNil(rows) || R.isNil(pages) || pages === 1, renderNothing),
  withProps(({ changeStart, rows }) => ({
    onChange: page => changeStart(rows * (page - 1)),
    onSubmit: page => changeStart(rows * (page - 1)),
    labels: {
      page: <FormattedMessage id="de.search.page" />,
      of: <FormattedMessage id="de.search.page.of" />,
      startPage: <FormattedMessage id="wcag.search.page.start" />,
      previousPage: <FormattedMessage id="wcag.search.page.previous" />,
      nextPage: <FormattedMessage id="wcag.search.page.next" />,
      endPage: <FormattedMessage id="wcag.search.page.end" />,
    },
  })),
)(Pagination);
