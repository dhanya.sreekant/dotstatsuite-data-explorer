import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { makeStyles } from '@material-ui/core/styles';
import { FormattedMessage } from 'react-intl';
import Typography from '@material-ui/core/Typography';
import { Logo, Spotlight, LabelDivider, CollapseButtons } from '@sis-cc/dotstatsuite-visions';
import Grid from '@material-ui/core/Grid';
import Loader from '../loader';
import Page from '../Page';
import { ID_HOME_PAGE } from '../../css-api';

const useStyles = makeStyles(theme => ({
  page: {
    backgroundColor: theme.palette.primary.main,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
  },
  container: {
    padding: theme.spacing(5, 1.25),
    flexWrap: 'nowrap', // edge
  },
  textColor: {
    color: theme.palette.common.white,
  },
  spotlight: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1),
  },
  content: {
    padding: '0 5%',
  },
}));

const Home = ({ isLoading, logo, intl, facets, changeConstraints, changeTerm, hasNoSearch }) => {
  const classes = useStyles();

  return (
    <Page id={ID_HOME_PAGE} classes={{ root: classes.page }}>
      <Grid item sm={12} md={7} container direction="column" className={classes.container}>
        <Logo logo={logo}>
          <Typography variant={'h6'} className={classes.textColor}>
            <FormattedMessage id="de.search.splash" />
          </Typography>
        </Logo>
        {hasNoSearch ? null : (
          <Grid container item xs={12} direction="column" className={classes.content}>
            {isLoading && <Loader />}
            {!isLoading && (
              <Fragment>
                <Grid item className={classes.spotlight}>
                  <Spotlight
                    hasClearAll
                    hasCommit
                    withBorder
                    fullWidth
                    action={changeTerm}
                    placeholder={intl.formatMessage({ id: 'vx.spotlight.placeholder' })}
                  />
                </Grid>
                {R.not(R.isEmpty(facets)) && (
                  <Grid item className={classes.facets}>
                    <LabelDivider
                      label={<FormattedMessage id="de.search.topics.browse" />}
                      withMargin
                    />
                    <CollapseButtons items={facets} action={changeConstraints} />
                  </Grid>
                )}
              </Fragment>
            )}
          </Grid>
        )}
      </Grid>
    </Page>
  );
};

Home.propTypes = {
  isLoading: PropTypes.bool,
  changeConstraints: PropTypes.func,
  changeTerm: PropTypes.func,
  facets: PropTypes.array,
  hasNoSearch: PropTypes.bool,
  logo: PropTypes.string,
  intl: PropTypes.object,
};

export default Home;
