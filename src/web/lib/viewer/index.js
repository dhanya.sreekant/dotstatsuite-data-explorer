import { theme, viewer } from '../settings';
import { Colors } from '../../theme/theme';

const fonts = { fontFamily: theme.visFont };
export const options = {
  ...viewer,
  fonts: {
    header: {
      disclaimer: { ...fonts, color: Colors.ORANGE },
      subtitle: fonts,
      title: fonts,
      uprs: fonts,
      tooltip: fonts,
    },
    chart: {
      axisLegend: fonts,
      chartLegend: fonts,
      tooltip: { primary: fonts, secondary: fonts },
    },
    footer: { copyright: fonts, source: fonts },
  },
};
