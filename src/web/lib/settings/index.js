import * as R from 'ramda';

export const i18n = R.propOr({}, 'i18n', window.SETTINGS);
export const search = R.propOr({}, 'search', window.SETTINGS);
export const share = R.propOr({}, 'share', window.SETTINGS);
export const theme = R.propOr({}, 'theme', window.SETTINGS);
export const ga = R.propOr({}, 'ga', window.SETTINGS);
export const sdmx = R.propOr({}, 'sdmx', window.SETTINGS);
export const chart = R.propOr({}, 'chart', window.SETTINGS);
export const viewer = R.propOr({}, 'viewer', window.SETTINGS);

export const shareEndpoint = R.prop('endpoint', share);
export const shareConfirmUrl = R.prop('confirmUrl', share);
export const defaultSearchRows = R.prop('defaultRows', search);

export const hasNoSearch = R.pipe(R.has('endpoint'), R.not)(search);

export const getAsset = id => R.path(['assets', id], window.SETTINGS);

export const locales = R.propOr({}, 'locales', i18n);
export const getLocaleId = id => (R.has(id, locales) ? id : R.prop('localeId', i18n));

export const indexedDatasources = R.pipe(
  R.prop('datasources'),
  R.toPairs,
  R.reduce((memo, [id, datasource]) => {
    if (R.prop('indexed', datasource)) return R.append(id, memo);
    return memo;
  }, []),
)(sdmx);
export const getDatasource = R.flip(R.prop)(R.prop('datasources', sdmx));
export const sdmxRange = R.prop('range', sdmx);
export const defaultFrequency = R.prop('frequency', sdmx);
export const sdmxPeriod = R.pathOr([], ['period', 'default'], sdmx);
export const sdmxPeriodBoundaries = R.pathOr([], ['period', 'boundaries'], sdmx);
export const sdmxPeriodBoundariesMinMax = {
  min: R.head(sdmxPeriodBoundaries),
  max: R.last(sdmxPeriodBoundaries),
};
export const getSdmxAttribute = id =>
  R.pipe(
    R.pathOr([], ['attributes', id]),
    R.ifElse(R.is(Array), R.identity, R.flip(R.append)([])),
  )(sdmx);
export const valueIcons = R.prop('valueIcons', sdmx);

export const chartOptions = R.prop('options', chart);
export const chartUrl = R.prop('url', chart);

export const mapOptions = R.pipe(
  R.path(['chart', 'maps']),
  R.values,
  R.map(value =>
    R.map(levelId => ({ id: `${value.id}:${levelId}`, mapId: value.id, levelId }), value.levels),
  ),
  R.unnest,
)(window.SETTINGS);

export const getMap = mapId => R.path(['chart', 'maps', mapId], window.SETTINGS);

export const cellsLimit = R.pathOr(0, ['table', 'cellsLimit'], window.SETTINGS);

export const customAttributes = R.propOr({}, 'attributes', sdmx);
export const units = R.propOr({}, 'units', sdmx);
