import * as Freq from '../frequency';

jest.mock('../../settings', () => ({
  defaultFrequency: 'A',
  sdmxPeriodBoundaries: ['2017', '2020'],
}));

describe('Frequency', () => {
  test('is valid Date', () => {
    expect(Freq.getSdmxPeriod('A')(new Date('2018'))).toEqual('2018');
    expect(Freq.getSdmxPeriod('S')(new Date('2018 01 01'))).toEqual('2018-S1');
    expect(Freq.getSdmxPeriod('Q')(new Date('2018 01 01'))).toEqual('2018-Q1');
    expect(Freq.getSdmxPeriod('M')(new Date('2018 01 01'))).toEqual('2018-01');
    expect(Freq.getSdmxPeriod('W')(new Date('2018 01 01'))).toEqual('2018-W01');
    expect(Freq.getSdmxPeriod('B')(new Date('2018 01 01'))).toEqual('2018-W01');
    expect(Freq.getSdmxPeriod('D')(new Date('2018 01 01'))).toEqual('2018-01-01');
    expect(Freq.getSdmxPeriod('H')(new Date('2018 01 01'))).toEqual('2018-01-01T00:00:00');
    expect(Freq.getSdmxPeriod('N')(new Date('2018 01 01'))).toEqual('2018-01-01T00:00:00');
  });
  test('parse sdmx periods to dates', () => {
    expect(Freq.getDateFromSdmxPeriod('A', ['2018', '2019'])).toEqual([
      new Date('2018-01-01T00:00:00'),
      new Date('2019-01-01T00:00:00'),
    ]);
    expect(Freq.getDateFromSdmxPeriod('A', [undefined, '2019'])).toEqual([
      undefined,
      new Date('2019-01-01T00:00:00'),
    ]);
    expect(Freq.getDateFromSdmxPeriod('A', [undefined, undefined])).toEqual([undefined, undefined]);
    expect(Freq.getDateFromSdmxPeriod('S', ['2018-S1', '2019-S1'])).toEqual([
      new Date('2018-01-01T00:00:00'),
      new Date('2019-01-01T00:00:00'),
    ]);
    expect(Freq.getDateFromSdmxPeriod('Q', ['2018-Q1', '2019-Q1'])).toEqual([
      new Date('2018-01-01T00:00:00'),
      new Date('2019-01-01T00:00:00'),
    ]);
    expect(Freq.getDateFromSdmxPeriod('W', ['2018-W1', '2023-W1'])).toEqual([
      new Date('2018-01-01T00:00:00'),
      new Date('2023 1 2'),
    ]);
    expect(Freq.getDateFromSdmxPeriod('Y', ['2018', '2019'])).toEqual([
      new Date('2018-01-01T00:00:00'),
      new Date('2019-01-01T00:00:00'),
    ]);
    expect(Freq.getDateFromSdmxPeriod('Y', ['2018-Q1', '2019-Q1'])).toEqual([undefined, undefined]);
    expect(Freq.getDateFromSdmxPeriod(undefined, ['2018', '2019'])).toEqual([
      new Date('2018-01-01T00:00:00'),
      new Date('2019-01-01T00:00:00'),
    ]);
    expect(Freq.getDateFromSdmxPeriod(undefined, ['2018-Q1', '2019-Q1'])).toEqual([
      undefined,
      undefined,
    ]);
  });
  test('counter of period following the frequency', () => {
    const boundaries = [new Date('2017'), new Date('2020')];
    const dates1 = [new Date('2018'), new Date('2019')];
    // const dates2 = [undefined, new Date('2019')];

    expect(Freq.getIntervalPeriod(boundaries)('A', dates1)).toEqual([2, 4]);
    expect(Freq.getIntervalPeriod(boundaries)('S', dates1)).toEqual([3, 7]);
    expect(Freq.getIntervalPeriod(boundaries)('Q', dates1)).toEqual([5, 13]);
    expect(Freq.getIntervalPeriod(boundaries)('W', dates1)).toEqual([53, 157]);
    expect(Freq.getIntervalPeriod(boundaries)(undefined, dates1)).toEqual([2, 4]);
    //expect(Freq.getIntervalPeriod(boundaries)(undefined, dates2)).toEqual([3, 4]);
    expect(Freq.getIntervalPeriod(boundaries)(undefined, [undefined, undefined])).toEqual([4, 4]);
  });
});
