import valueParser from '../valueParser';
import md5 from 'md5';
import bs62 from 'bs62';

describe('valueParser', () => {
  it('should pass, no id', () => {
    expect(valueParser({ facetId: 1 })({ val: 'test', count: 10 })).toEqual({
      id: 'test',
      label: 'test',
      count: 10,
      isSelected: false,
      svgPath: undefined,
    });
  });

  const value = {
    val: '0|Economy#ECO#',
    label: 'Economy#ECO#',
    count: 10,
  };

  it('should pass, no constraint', () => {
    expect(valueParser({ facetId: 1 })(value)).toEqual({
      id: value.val,
      label: 'Economy (ECO)',
      count: value.count,
      isSelected: false,
      svgPath: undefined,
    });
  });

  it('should pass and be selected', () => {
    expect(
      valueParser({ facetId: 1, constraints: { [md5(`${1}${value.val}`)]: 111 } })(value),
    ).toEqual({
      id: value.val,
      label: 'Economy (ECO)',
      count: value.count,
      isSelected: true,
      svgPath: undefined,
    });
  });

  it('should pass, with an icon', () => {
    expect(
      valueParser({ facetId: bs62.encode('1'), config: { valueIcons: { 1: { ECO: 'icon' } } } })(
        value,
      ),
    ).toEqual({
      id: value.val,
      label: 'Economy (ECO)',
      count: value.count,
      isSelected: false,
      svgPath: 'icon',
    });
  });
});
