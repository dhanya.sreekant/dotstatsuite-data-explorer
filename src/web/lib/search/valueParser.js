import * as R from 'ramda';
import md5 from 'md5';
import bs62 from 'bs62';
import C from './constants';

const setFormat = (string = '') =>
  R.converge((match, str) => R.replace(R.head(match), ` (${R.last(match)})`, str), [
    R.match(C.VALUE_MASK),
    R.identity,
  ])(`${string}`);

const extractId = R.pipe(
  R.match(C.FACET_VALUE_MASK),
  R.last,
  R.ifElse(R.isNil, R.always(''), R.identity),
  R.tail,
  R.dropLast(1),
);

export default options => ({ val, count, label }) => ({
  id: val,
  label: setFormat(R.defaultTo(val, label)),
  count,
  isSelected: R.has(md5(`${options.facetId}${val}`), R.defaultTo({}, options.constraints)),
  // should be at rendering level
  svgPath: R.path(
    ['valueIcons', bs62.decode(`${options.facetId}`), extractId(val)],
    options.config,
  ),
});
