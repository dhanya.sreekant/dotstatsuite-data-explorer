import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import '@babel/polyfill';
import * as R from 'ramda';
import qs from 'qs';
import Helmet from './components/helmet';
import { I18nProvider } from './i18n';
import { history, Switch } from './router';
import { createStore, reducer } from './core';
import sagas from './sagas';
import { ThemeProvider } from './theme';
import Vis from './components/vis';
import Search from './components/search';
import App from './components/App';
import ErrorBoundary from './components/error';
import { changeLocation } from './ducks/router';
import { getLocation } from './selectors/router';
import { initialize as initializeAnalytics } from './utils/analytics';
import { initialize as initializeI18n } from './i18n';
import searchApi from './api/search';
import * as Settings from './lib/settings';
import meta from '../../package.json';
import keycloakAdapter from 'keycloak-js';
import { KeycloakProvider } from 'react-keycloak';
import { userSignIn, pushToken, deleteToken, AUTHENTICATED } from './ducks/user';

console.info(`${meta.name}@${meta.version}`); // eslint-disable-line no-console

const initialState = {};
const store = createStore(initialState, history, Settings.ga, reducer, sagas);
const routes = { '/': Search, '/vis': Vis };

const historian = (location, action) => {
  const prevPathname = R.prop('pathname', getLocation(store.getState()));
  const nextPathname = R.prop('pathname')(location);
  if (action !== 'POP' && R.equals(prevPathname, nextPathname)) return;
  const params = qs.parse(R.prop('search')(location), { ignoreQueryPrefix: true, comma: true });
  store.dispatch(changeLocation(location, params));
};

searchApi.setConfig(Settings.search);

initializeAnalytics(Settings.ga);
initializeI18n(Settings.i18n);

history.listen(historian);
historian(history.location, 'POP');

const { keycloak: keycloakOIDC } = window.CONFIG;
const keycloak = keycloakAdapter(keycloakOIDC);
const onKeycloakEvent = (event, error) => {
  if (event === 'onAuthSuccess') {
    keycloak.loadUserInfo().then(userInfo => store.dispatch(userSignIn(userInfo)));
  }
  if (event === 'onReady') {
    store.dispatch({ type: AUTHENTICATED });
  }
  if (event === 'onTokenExpired') {
    store.dispatch(deleteToken());
  }
  console.log('[KEYCLOAK] onKeycloakEvent', event, error); // eslint-disable-line no-console
};

const onKeycloakTokens = tokens => {
  store.dispatch(pushToken(tokens.idToken));
  console.log('onKeycloakTokens', tokens); // eslint-disable-line no-console
};

render(
  <KeycloakProvider
    keycloak={keycloak}
    onEvent={onKeycloakEvent}
    onTokens={onKeycloakTokens}
    initConfig={{ onLoad: 'check-sso', promiseType: 'native' }}
  >
    <ErrorBoundary>
      <Provider store={store}>
        <ThemeProvider theme={Settings.theme}>
          <I18nProvider messages={window.I18N}>
            <ErrorBoundary>
              <Helmet />
              <App>
                <Switch routes={routes} />
              </App>
            </ErrorBoundary>
          </I18nProvider>
        </ThemeProvider>
      </Provider>
    </ErrorBoundary>
  </KeycloakProvider>,
  document.getElementById('root'),
);
