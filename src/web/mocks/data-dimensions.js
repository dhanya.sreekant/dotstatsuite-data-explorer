export const output1 = {
  many: {
    LOCATION: {
      id: 'LOCATION',
      index: 1,
      keyPosition: 1,
      label: 'Country',
      name: 'Country',
      role: 'REF_AREA',
      values: [
        {
          id: 'AUS',
          index: 0,
          label: 'Australia',
          name: 'Australia',
        },
        {
          id: 'AUT',
          index: 1,
          label: 'Austria',
          name: 'Austria',
        },
        {
          id: 'BEL',
          index: 2,
          label: 'Belgium',
          name: 'Belgium',
        },
        {
          id: 'CAN',
          index: 3,
          label: 'Canada',
          name: 'Canada',
        },
        {
          id: 'CZE',
          index: 4,
          label: 'Czech Republic',
          name: 'Czech Republic',
        },
        {
          id: 'DNK',
          index: 5,
          label: 'Denmark',
          name: 'Denmark',
        },
        {
          id: 'FIN',
          index: 6,
          label: 'Finland',
          name: 'Finland',
        },
        {
          id: 'FRA',
          index: 7,
          label: 'France',
          name: 'France',
        },
        {
          id: 'CHL',
          index: 8,
          label: 'Chile',
          name: 'Chile',
        },
        {
          id: 'EST',
          index: 9,
          label: 'Estonia',
          name: 'Estonia',
        },
      ],
    },
    SUBJECT: {
      id: 'SUBJECT',
      index: 0,
      keyPosition: 0,
      label: 'Subject',
      name: 'Subject',
      values: [
        {
          id: 'PRINTO01',
          index: 0,
          label: 'Industrial production, s.a.',
          name: 'Industrial production, s.a.',
        },
        {
          id: 'PRMNTO01',
          index: 1,
          label: 'Total manufacturing, s.a.',
          name: 'Total manufacturing, s.a.',
        },
      ],
    },
  },
  one: {
    FREQUENCY: {
      id: 'FREQUENCY',
      index: 3,
      keyPosition: 3,
      label: 'Frequency',
      name: 'Frequency',
      role: 'FREQ',
      values: [
        {
          id: 'A',
          index: 0,
          label: 'Annual',
          name: 'Annual',
        },
      ],
    },
    MEASURE: {
      id: 'MEASURE',
      index: 2,
      keyPosition: 2,
      label: 'Measure',
      name: 'Measure',
      values: [
        {
          id: 'GP',
          index: 0,
          label: 'Growth previous period',
          name: 'Growth previous period',
        },
      ],
    },
    TIME_PERIOD: {
      id: 'TIME_PERIOD',
      index: 4,
      label: 'Time',
      name: 'Time',
      role: 'TIME_PERIOD',
      values: [
        {
          id: '2015',
          index: 0,
          label: '2015',
          name: '2015',
        },
      ],
    },
  },
  zero: {},
};
