import numeral from 'numeral';
import * as R from 'ramda';

import '@formatjs/intl-pluralrules/polyfill';
import '@formatjs/intl-pluralrules/dist/locale-data/ar';
import '@formatjs/intl-pluralrules/dist/locale-data/en';
import '@formatjs/intl-pluralrules/dist/locale-data/fr';
import '@formatjs/intl-pluralrules/dist/locale-data/es';
import '@formatjs/intl-pluralrules/dist/locale-data/it';
import '@formatjs/intl-pluralrules/dist/locale-data/km';
import '@formatjs/intl-pluralrules/dist/locale-data/nl';

// https://fr.wiktionary.org/wiki/Wiktionnaire:BCP_47/language-2

/*// For IE11: yarn add @formatjs/intl-pluralrules
if (!Intl.PluralRules) {
  import '@formatjs/intl-pluralrules/polyfill';
  import '@formatjs/intl-pluralrules/dist/locale-data/ar';
  import '@formatjs/intl-pluralrules/dist/locale-data/en';
  import '@formatjs/intl-pluralrules/dist/locale-data/fr';
  import '@formatjs/intl-pluralrules/dist/locale-data/es';
  import '@formatjs/intl-pluralrules/dist/locale-data/it';
  import '@formatjs/intl-pluralrules/dist/locale-data/km';
  import '@formatjs/intl-pluralrules/dist/locale-data/nl';
}*/

/*// For IE11: yarn add @formatjs/intl-relativetimeformat
if (!Intl.RelativeTimeFormat) {
  import '@formatjs/intl-relativetimeformat/polyfill';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/ar';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/en';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/fr';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/es';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/it';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/km';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/nl';
}*/

const model = locale => `${locale}/${locale}`;

export const setLocale = locale => numeral.locale(model(locale));

export const initialize = ({ locales = [], localeId = 'en' }) => {
  setLocale(localeId);

  R.forEach(locale => {
    const delimiters = R.prop('delimiters')(locale);
    if (R.isNil(delimiters)) return;
    numeral.register('locale', model(R.prop('id')(locale)), { delimiters });
  }, R.values(locales));
};

export { default as I18nProvider } from './provider';
export { default as withLocale } from './with-locale';
