import { isNil } from 'ramda';
import { select, take } from 'redux-saga/effects';
import { PUSH_TOKEN } from '../ducks/user';
import { getTokenId, getMe } from '../selectors/user';

export function* getTokenIdUtil() {
  let tokenId = yield select(getTokenId);
  const me = yield select(getMe);
  if (isNil(tokenId) && !!me) {
    yield take(PUSH_TOKEN);
    tokenId = yield select(getTokenId);
  }
  return tokenId;
}
