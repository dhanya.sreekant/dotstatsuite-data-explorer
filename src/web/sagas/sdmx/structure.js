import axios from 'axios';
import * as R from 'ramda';
import { select, put, call, takeLatest } from 'redux-saga/effects';
import {
  getActualContentConstraintsDefaultSelection,
  getActualContentConstrainedTimePeriod,
  parseStructure as sdmxjsParseStructure,
  getDataquery,
} from '@sis-cc/dotstatsuite-sdmxjs';
import { getStructureRequestArgs } from '../../selectors/sdmx';
import { getConstraints, getDataAvailability, getLocale } from '../../selectors/router';
import { getVisLayout } from '../../selectors';
import { changeFilter } from '../../ducks/router';
import { HANDLE_STRUCTURE, REQUEST_STRUCTURE, PARSE_STRUCTURE } from '../../ducks/sdmx';
import { setPending, pushLog } from '../../ducks/app';
import { getDefaultSelection } from '../../lib/search/sdmx-constraints-bridge';
import { getDefaultRouterParams, getDefaultSelectionContentConstrained, getOnlyHasDataDimensions } from '../../lib/sdmx';
import { getTokenIdUtil } from '../token';

const isDev = process.env.NODE_ENV === 'development';

function* requestStructure() {
  const pendingId = 'getStructure';

  try {
    const { url, headers, params } = yield select(getStructureRequestArgs);
    yield put(setPending(pendingId, true));
    if (isDev) console.info(`request: ${pendingId}`); // eslint-disable-line no-console
    const tokenId = yield call(getTokenIdUtil);
    const authHeaders = R.when(
      R.always(!R.isNil(tokenId)),
      R.assoc('Authorization', `Bearer ${tokenId}`),
    )(headers);
    const response = yield call(axios.get, url, { headers: authHeaders, params });
    const structure = response.data;
    yield put(setPending(pendingId, false));
    yield put({ type: PARSE_STRUCTURE, structure });
  } catch (error) {
    yield put(setPending(pendingId, false));

    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.log(error.response.data); // eslint-disable-line no-console
      console.log(error.response.status); // eslint-disable-line no-console
      console.log(error.response.headers); // eslint-disable-line no-console
      yield put(pushLog({ type: 'response', log: { ...error.response } }));
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request); // eslint-disable-line no-console
      yield put(pushLog({ type: 'request', log: error }));
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message); // eslint-disable-line no-console
      yield put(pushLog({ type: 'error', log: error }));
    }
  }
}


function* parseStructure({ structure }) {
  const locale = yield select(getLocale);
  try {
    const {
      dimensions,
      frequency,
      layout,
      params,
      selection,
      name,
      timePeriod,
      externalResources,
      contentConstraints,
    } = sdmxjsParseStructure(structure, locale);
    const constraints = yield select(getConstraints);
    const dataAvailabilityIsOff = yield select(getDataAvailability);
    const currentLayout = yield select(getVisLayout);
    const hasContentConstraints = R.not(R.isNil(contentConstraints))
    const hasDataAvailability = R.and(hasContentConstraints, R.not(dataAvailabilityIsOff));
    
    const structureSelection = hasDataAvailability
      ? getActualContentConstraintsDefaultSelection({ selection, contentConstraints })
      : selection;

    const contentConstrainedTimePeriodBoundaries = getActualContentConstrainedTimePeriod(
      contentConstraints,
    );

    const defaultSelection = hasDataAvailability 
      ? R.pipe(
          getOnlyHasDataDimensions,
          (dimensions) => getDefaultSelection(dimensions, structureSelection, constraints), 
          getDefaultSelectionContentConstrained(contentConstraints),
          R.reject(R.isEmpty),
        )(dimensions)
      : getDefaultSelection(dimensions, structureSelection, constraints);
        
    yield put({
      type: HANDLE_STRUCTURE,
      structure: {
        dimensions,
        contentConstraints,
        hasContentConstraints,
        frequencyArtefact: frequency,
        layout: R.defaultTo(currentLayout, layout),
        externalResources,
        params: getDefaultRouterParams({
          frequencyArtefact: frequency,
          params,
          dataquery: getDataquery(
            dimensions,
            defaultSelection
          ),
        }),
        title: name,
        timePeriod,
        timePeriodBoundaries: R.prop('boundaries', contentConstrainedTimePeriodBoundaries),
        timePeriodIncludingBoundaries: R.prop(
          'includingBoundaries',
          contentConstrainedTimePeriodBoundaries,
        ),
      },
      pushHistory: '/vis',
    });
    yield put(changeFilter());
  } catch (error) {
    yield put(pushLog({ type: 'parse', log: error }));
  }
}

export function* watchRequestStructure() {
  yield takeLatest(REQUEST_STRUCTURE, requestStructure);
}

export function* watchParseStructure() {
  yield takeLatest(PARSE_STRUCTURE, parseStructure);
}
