import { createMuiTheme } from '@material-ui/core/styles';

const highlight1 = '#f7a42c'; // orange
const highlight2 = '#8CC841'; // green

export const Colors = {
  GREY1: '#F0F0F0',
  GREY2: '#CCCCCC',
  GREY3: '#494444',
  GREY4: '#A4A1A1',
  GREY5: '#666666',
  BLUE1: '#2973BD',
  BLUE2: '#0965C1',
  BLUE3: '#2F75B5',
  BLUE4: '#BDD7EE',
  BLUE5: '#DDEBF7',
  BLUE6: '#1E226A',
  WHITE1: '#FFFFFF',
  BLACK1: '#000000',
  GREEN1: '#8CC841',
  ORANGE: '#f7a32c',
};

export const FontSizes = {
  SIZE1: 18,
  SIZE2: 14,
  SIZE3: 12,
};

export const Formats = {
  WIDE: 'initial',
  NARROW: 400,
  APP: 1000,
  APP_MIN_SIZE: 360,
  ITEM_MIN_SIZE: 350,
};

export const Layout = {
  PADDING: '5%',
  SIDE_WIDTH: 300,
  FILTER_MAX_HEIGHT: 250,
};

export const theme = (rtl = 'ltr') =>
  createMuiTheme({
    direction: rtl,
    overrides: {
      MuiButton: {
        root: {
          textTransform: 'none',
        },
      },
    },
    palette: {
      action: {
        active: 'rgba(14, 144, 144, 0.54)',
        selected: 'rgba(14, 144, 224, 0.25)',
        hover: 'rgba(14, 144, 224, 0.13)',
      },
      primary: {
        main: '#0965c1',
        light: '#0e90e0',
        dark: '#0549ab',
      },
      secondary: {
        main: '#e3e9ed',
        light: '#f5f8fa',
        dark: '#ebf1f5',
      },
      tertiary: {
        light: '#e2f2fb',
        dark: '#b7def6',
      },
      button: {
        mainOpacity: 'rgba(0, 0, 0, 0.2)',
      },
      highlight: {
        hl1: highlight1,
        hl2: highlight2,
      },
      grey: {
        200: '#F3F7FB',
        300: '#cccccc',
        700: '#666666',
        A700: '#182026',
      },
    },
    mixins: {
      dataflow: {
        fontWeight: 400,
        fontFamily: "'Roboto Slab', serif",
      },
      scopeList: {
        fontWeight: 700,
        fontFamily: "'PT Sans Narrow', 'Helvetica Neue', Helvetica, Arial, sans-serif",
      },
      excel: {
        headerFont: '#ffffff',
        sectionFont: '#000000',
        rowFont: '#000000',
      },
      sisccButton: {
        '&:hover': {
          backgroundColor: highlight1,
        },
      },
    },
    typography: {
      fontFamily: "'Helvetica Neue', Helvetica, Arial, sans-serif",
      h6: {
        fontSize: ' 1.0625rem',
      },
    },
  });
