import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { mergeDeepRight } from 'ramda';
import { ThemeProvider as GlamThemeProvider } from 'glamorous';
import { mainTheme } from '@sis-cc/dotstatsuite-ui-components';
import { ThemeProvider } from '@material-ui/core/styles';
import { getIsRtl } from '../selectors/router';
import { theme as muiTheme } from './theme';
//import { theme as muiTheme } from '@sis-cc/dotstatsuite-visions';
import { Rtl } from './jss-provider';

const Provider = ({ theme, children, isRtl }) => (
  <Rtl>
    <ThemeProvider theme={mergeDeepRight(muiTheme(isRtl ? 'rtl' : 'ltr'), theme)}>
      <GlamThemeProvider theme={mergeDeepRight(mainTheme, theme)}>
        {React.Children.only(children)}
      </GlamThemeProvider>
    </ThemeProvider>
  </Rtl>
);

Provider.propTypes = {
  theme: PropTypes.object,
  children: PropTypes.element.isRequired,
  isRtl: PropTypes.bool,
};

Provider.defaultProps = {
  theme: mainTheme,
  children: PropTypes.element.isRequired,
};

export default connect(createStructuredSelector({ isRtl: getIsRtl }))(Provider);
