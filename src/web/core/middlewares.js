import * as R from 'ramda';
import { CHANGE_LOCATION } from '../ducks/router';
import { requestSearch } from '../ducks/search';
import { REQUEST_DATA } from '../ducks/sdmx';
import { getQuery } from '../utils/router';
import { getParams } from '../selectors/router';

const isDev = process.env.NODE_ENV === 'development';

export const historyMiddleware = history => store => next => action => {
  if (R.not(R.has('pushHistory', action))) return next(action);

  const future = next(action);

  const search = getQuery(getParams(store.getState()));

  history.push({
    pathname: R.prop('pushHistory', action),
    search,
  });

  // eslint-disable-next-line no-console
  if (isDev) console.info(`historyMiddleware: ${action.type}`);

  return future;
};

export const requestMiddleware = store => next => action => {
  const request = R.prop('request', action);
  const future = next(action);

  if (R.isNil(request) || action.type === CHANGE_LOCATION) return future;

  switch (request) {
    case 'getSearch':
      store.dispatch(requestSearch());
      break;
    case 'getData':
      store.dispatch({ type: REQUEST_DATA });
      break;
    case 'getStructure':
      store.dispatch({ type: REQUEST_DATA, shouldRequestStructure: true });
      break;
    default:
      // eslint-disable-next-line no-console
      if (isDev) console.log(`requestMiddleware: unknown request ${request}`);
      return future;
  }

  // eslint-disable-next-line no-console
  if (isDev) console.info(`requestMiddleware: ${action.type} -> ${request}`);

  return future;
};

/*const analyticsAction = new Set([CHANGE_LOCATION, CHANGE_VIEWER_ID, FETCH_DATA, SHARE_CHART]);
export const analyticsMiddleware = ({ getState }) => next => action => {
  if (analyticsAction.has(action.type)) {
    const future = next(action);
    // eslint-disable-next-line no-console
    if (isDev) console.info(`analyticsMiddleware: ${action.type}`);
    const isVis = R.pipe(
      R.path(['payload', 'location', 'pathname']),
      R.equals('/vis'),
    )(action);
    //const getter = isVis ? getVisPath : getSearchPath;

    // TEMP
    const apiId = R.pathOr('', ['api', 'id'])(action);
    const dataflow = R.either(R.equals('CSV_FULL'), R.equals('CSV_SELECTION'))(apiId)
      ? getDataflowAsString({ withName: true })(getState())
      : R.replace('CSV_FULL/', '')(apiId);

    const options = {
      viewerId: getVisViewerId()(getState()),
      label: R.isNil(action.payload) ? null : action.payload.label, // SHARE_CHART
      isDownload: R.path(['api', 'expectFile'])(action), // FETCH_DATA
      //url: getter()(getState()), // CHANGE_LOCATION
      url: 'refactoring in progress',
      isVis: isVis, // CHANGE_LOCATION
      dataflow, // CHANGE_LOCATION
    };

    event(action.type, options);
    return future;
  }
  return next(action);
};*/
