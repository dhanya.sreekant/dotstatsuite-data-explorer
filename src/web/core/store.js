import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { historyMiddleware, requestMiddleware } from './middlewares';
import createSagaMiddleware from 'redux-saga';

export const withLogger = middlewares => {
  if (process.env.NODE_ENV === 'production') return middlewares;

  const logger = createLogger({
    duration: true,
    timestamp: false,
    collapsed: true,
    diff: true,
  });

  return [...middlewares, logger];
};

export default (initialState = {}, history, { token }, rootReducer, rootSaga) => {
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [thunk, sagaMiddleware, historyMiddleware(history), requestMiddleware];

  if (token) {
    // eslint-disable-next-line no-console
    if (process.env.NODE_ENV === 'development') console.warn(`analyticsMiddleware: desactivated`);

    //middlewares.push(analyticsMiddleware);
  }

  const enhancers = [applyMiddleware(...withLogger(middlewares))];

  const store = createStore(rootReducer, initialState, compose(...enhancers));

  sagaMiddleware.run(rootSaga);

  return store;
};
