import { combineReducers } from 'redux';
import router from '../ducks/router';
import app from '../ducks/app';
import search from '../ducks/search';
import vis from '../ducks/vis';
import sdmx from '../ducks/sdmx';
import user from '../ducks/user';

export default combineReducers({
  router, // tailor made, i am way too stupid to understand and see the benefits of react-router-*
  app,
  search,
  sdmx,
  vis,
  user,
});
