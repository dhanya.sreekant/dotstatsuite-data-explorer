import { compose, mapProps, withProps } from 'recompose';
import * as R from 'ramda';
import { rules } from '@sis-cc/dotstatsuite-components';
import { options as config } from '../lib/viewer';

export const withTableProps = withProps(props => ({
  tableProps: R.pipe(
    R.assoc('config', config),
    R.assoc('footnotes', R.path(['chartData', 'footnotes'], props)),
    R.assoc('isNotATable', R.prop('isNotATable', props)),
    R.assoc('headerProps', R.prop('headerProps', props)),
  )(R.propOr({}, 'tableProps', props)),
}));

export const withChartProps = withProps(props => ({
  chartData: R.merge(R.propOr({}, 'chartData', props), R.propOr({}, 'headerProps', props)),
}));

export const withChartShareData = withProps(props => ({
  chartShareData: rules.getChartShareData(
    R.prop('chartData', props),
    R.prop('chartOptions', props),
    R.merge(config, R.pick(['display', 'formaterIds', 'locale', 'sourceHeaders'], props)),
    R.prop('type', props),
    R.prop('shareMode', props),
    R.prop('range', props),
  ),
}));

export const withTableShareData = withProps(props => ({
  tableShareData: rules.getTableShareData({
    ...props,
    config: R.merge(config, R.pick(['display', 'locale', 'sourceHeaders'], props)),
  }),
}));

export const withShareData = mapProps(
  ({ chartShareData, shareMode, tableShareData, type, ...rest }) => ({
    shareData: {
      data: type === 'table' ? tableShareData : chartShareData,
      share: shareMode,
      type,
    },
    type,
    ...rest,
  }),
);

export const withToolProps = mapProps(({ properties, share, shareData, ...rest }) => ({
  toolsProps: {
    properties,
    share: () => share(shareData),
  },
  ...rest,
}));

export const withVisDataProps = mapProps(
  ({
    chartData,
    chartOptions,
    footerProps,
    fonts,
    headerProps,
    isLoadingData,
    isLoadingMap,
    isNonIdealState,
    isNonIdealMapState,
    tableProps,
    toolsProps,
    type,
    hasSelection,
    hasAccessibility
  }) => ({
    isLoadingData,
    isLoadingMap,
    toolsProps,
    viewerProps: {
      ...(type === 'table' ? { tableProps } : { chartData, chartOptions, chartConfig: config }),
      footerProps,
      fonts,
      headerProps,
      isNonIdealState,
      isNonIdealMapState,
      type,
    },
    hasSelection,
    hasAccessibility
  }),
);

export default compose(
  withChartProps,
  withTableProps,
  withChartShareData,
  withTableShareData,
  withShareData,
  withToolProps,
  withVisDataProps,
);
